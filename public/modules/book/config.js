var bookStat = angular.module("bookStat", [
  "ngResource",
  "ui.router",
  "wsCommonDirectives",
  "wsCommonServices",
]);

bookStat.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/word");
  $stateProvider
    .state("word", {
      url: "/word?sourceName",
      templateUrl: "/modules/book/templates/word.html",
      controller: "WordCtrl",
    })
    .state("bigram", {
      url: "/bigram?sourceName",
      templateUrl: "/modules/book/templates/bigram.html",
      controller: "BigramCtrl",
    })
    .state("upload", {
      url: "/upload",
      templateUrl: "/modules/common/templates/upload.html",
      controller: "UploadCtrl",
    });
});

bookStat.run([
  "$location",
  "$rootScope",
  function($location, $rootScope) {
    $rootScope.$on("$stateChangeSuccess", function(event, current, previous) {
      if (current.data && current.data.title) {
        $rootScope.title = current.data.title;
      }
    });
  },
]);
