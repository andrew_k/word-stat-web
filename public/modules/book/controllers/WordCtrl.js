var bookStat = angular.module("bookStat");

bookStat.controller("WordCtrl", function(
  $q,
  $rootScope,
  $scope,
  $state,
  $stateParams,
  $interval,
  $timeout,
  WordFrequency,
  TextEntityTitles,
) {
  $rootScope.title = ["Частота слов"].join(" ");

  $scope.wordFrequency = [];

  $scope.totalWords = 0;

  var limit = 100;
  $scope.skip = 0;

  $scope.search = $stateParams.search || undefined;
  $scope.sortDir = -1;
  $scope.total = 0;
  $scope.uniq = 0;
  $scope.popularFirst = true;
  $scope.groupByStem = false;

  $scope.selectedRange = undefined;

  $scope.nostopwords = 0;

  $scope.sourceName = $stateParams.sourceName || null;

  TextEntityTitles.query(function(result) {
    $scope.sourceNames = result;
    if (!$scope.sourceName) $scope.sourceName = result[0];
  });

  $scope.$watchGroup(
    ["search", "sourceName", "nostopwords", "popularFirst", "groupByStem"],
    function(val) {
      if (!$scope.sourceName) return;

      $scope.skip = 0;

      var frequency = WordFrequency.get(
        {
          search: $scope.search,
          skip: $scope.skip,
          limit: limit,
          sortDir: $scope.popularFirst === true ? -1 : 1,
          sourceName: $scope.sourceName,
          nostopwords: $scope.nostopwords ? 1 : 0,
          groupBy: $scope.groupByStem === true ? "stem" : "word",
        },
        function(result) {
          $scope.wordFrequency = result.words;
          $scope.total = result.total;
          $scope.uniq = result.uniq;
        },
      );

      frequency.$promise.then(function() {
        console.log("All promises loaded");
        $state.go(
          "word",
          {
            search: $scope.search,
            sourceName: $scope.sourceName,
          },
          {
            notify: false,
          },
        );
      });
    },
  );

  $scope.$watch("skip", function(val) {
    if (!val) return;

    WordFrequency.get(
      {
        search: $scope.search,
        skip: $scope.skip,
        limit: limit,
        sortDir: $scope.popularFirst === true ? -1 : 1,
        sourceName: $scope.sourceName,
        nostopwords: $scope.nostopwords ? 1 : 0,
        groupBy: $scope.groupByStem === true ? "stem" : "word",
      },
      function(result) {
        $scope.wordFrequency = $scope.wordFrequency.concat(result.words);
      },
    );
  });

  $(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      $scope.skip += limit;
      $scope.$apply();
    }
  });

  $scope.onFrequencyChartBarClick = function(e, d) {
    $scope.search = "^" + d.word + "$";
    $(window).scrollTop(0);
    $scope.$apply();
  };
});
