var bookStat = angular.module("bookStat");

bookStat.controller("UploadCtrl", function($rootScope, $scope, TextEntity) {
  $rootScope.title = ["Загрузить что-то"].join(" ");

  $scope.title = "";
  $scope.text = "";
  $scope.message = "";
  $scope.error = "";

  $scope.submit = function() {
    $scope.message = "";
    $scope.error = "";
    if ($scope.title && $scope.text) {
      var text = new TextEntity({
        title: $scope.title,
        text: $scope.text,
      });
      text.$save(function(res) {
        console.log(res);
        if (res.errmsg) {
          $scope.error = res.errmsg;
        } else {
          $scope.text = "";
          $scope.title = "";
          $scope.message = "Текст создан!";
        }
      });
    }
  };
});
