var bookStat = angular.module("bookStat");

bookStat.controller("BigramCtrl", function(
  $q,
  $rootScope,
  $scope,
  $state,
  $stateParams,
  $interval,
  $timeout,
  BigramFrequency,
  TextEntityTitles,
) {
  $rootScope.title = ["Частота слов"].join(" ");

  $scope.bigrams = [];

  $scope.totalWords = 0;

  var limit = 100;
  $scope.skip = 0;

  $scope.search = $stateParams.search || undefined;
  $scope.sortDir = -1;
  $scope.total = 0;
  $scope.uniq = 0;
  $scope.popularFirst = true;
  $scope.groupByStem = false;

  $scope.selectedRange = undefined;

  $scope.nostopwords = 0;

  $scope.sourceName = $stateParams.sourceName || null;

  TextEntityTitles.query(function(result) {
    $scope.sourceNames = result;
    if (!$scope.sourceName) $scope.sourceName = result[0];
  });

  $scope.$watchGroup(["search", "sourceName", "popularFirst"], function(val) {
    if (!$scope.sourceName) return;

    $scope.skip = 0;

    var frequency = BigramFrequency.get(
      {
        search: $scope.search,
        skip: $scope.skip,
        limit: limit,
        sortDir: $scope.popularFirst === true ? -1 : 1,
        sourceName: $scope.sourceName,
        nostopwords: $scope.nostopwords ? 1 : 0,
      },
      function(result) {
        $scope.bigrams = _.map(result.bigrams, convertBigram);
        $scope.total = result.total;
        $scope.uniq = result.uniq;
      },
    );

    frequency.$promise.then(function() {
      console.log("All promises loaded");
      $state.go(
        "bigram",
        {
          search: $scope.search,
          sourceName: $scope.sourceName,
        },
        {
          notify: false,
        },
      );
    });
  });

  $scope.$watch("skip", function(val) {
    if (!val) return;

    BigramFrequency.get(
      {
        search: $scope.search,
        skip: $scope.skip,
        limit: limit,
        sortDir: $scope.popularFirst === true ? -1 : 1,
        sourceName: $scope.sourceName,
      },
      function(result) {
        $scope.bigrams = _.uniq(
          $scope.bigrams.concat(_.map(result.bigrams, convertBigram)),
          false,
          function(a, b) {
            return a.word;
          },
        );
      },
    );
  });

  function convertBigram(bi) {
    return {
      word: bi._id["stem1"] + " " + bi._id["stem2"],
      count: bi.count,
      words: bi.words1.concat(bi.words2),
    };
  }

  $(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      $scope.skip += limit;
      $scope.$apply();
    }
  });

  // $scope.onFrequencyChartBarClick = function(e, d) {
  //     $scope.search = "^" + d.word + '$';
  //     $(window).scrollTop(0);
  //     $scope.$apply();
  // };
});
