var newsStat = angular.module("newsStat");

newsStat.controller("ArticleDistributionCtrl", function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  ArticleDistribution,
  appConstants,
) {
  $rootScope.title = [
    "Распределение статей ",
    '"' + $stateParams.word + '"',
  ].join(" ");
  $scope.articlesDistribution = [];

  ArticleDistribution.query({}, function(result) {
    $scope.articlesDistribution = result;
  });

  $scope.onBarClick = function(e, d) {};
});
