var newsStat = angular.module("newsStat");

newsStat.controller("WordDistributionCtrl", function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  WordDistribution,
  appConstants,
) {
  $rootScope.title = [
    "Распределение слова ",
    '"' + $stateParams.word + '"',
  ].join(" ");
  $scope.wordsFrequency = [];
  $scope.allWordsFrequency = [];
  $scope.word = $stateParams.word;

  WordDistribution.query(
    {
      search: $stateParams.word,
      timePeriod: "month",
    },
    function(result) {
      $scope.wordsFrequency = result;
    },
  );

  $scope.onBarClick = function(e, d) {
    if (d3.event.ctrlKey) {
      var win = window.open(
        $state.href("message_search", {
          search: $scope.word,
          start: d.date,
          end: d.date + appConstants.monthInMsec,
        }),
        "_blank",
      );
      win.focus();
    } else {
      $state.go("message_search", {
        search: $scope.word,
        sender: "andrew_k49",
        start: d.date,
        end: d.date + appConstants.monthInMsec,
      });
    }
  };
});
