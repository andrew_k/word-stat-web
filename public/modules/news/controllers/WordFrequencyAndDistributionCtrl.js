var newsStat = angular.module("newsStat");

newsStat.controller("WordFrequencyAndDistributionCtrl", function(
  $q,
  $rootScope,
  $scope,
  $state,
  $stateParams,
  $interval,
  $timeout,
  WordFrequency,
  WordDistribution,
  WordsTimeRange,
  WordInclusion,
  appConstants,
) {
  $rootScope.title = ["Общая статистика"].join(" ");

  $scope.wordFrequency = [];
  $scope.wordDistribution = [];
  $scope.wordsTimeRange = {};
  $scope.wordInclusion = [];

  $scope.totalWords = 0;

  var limit = 100;
  $scope.skip = 0;

  $scope.search = $stateParams.search || undefined;
  $scope.sortDir = -1;
  $scope.total = 0;
  $scope.start = 0;
  $scope.end = 0;

  $scope.selectedRange = undefined;
  $scope.rangeStep = appConstants.monthInMsec;

  $scope.sourceName = "korr";
  $scope.nostopwords = 0;

  // $interval(function() {
  //     $scope.selectedRange += $scope.rangeStep;
  // }, 1000);
  $scope.$watchGroup(
    ["search", "selectedRange", "sourceName", "nostopwords"],
    function(val) {
      $scope.skip = 0;

      var promise1 = WordFrequency.get(
        {
          search: $scope.search,
          skip: $scope.skip,
          limit: limit,
          sortDir: $scope.sortDir,
          sourceName: $scope.sourceName,
          nostopwords: $scope.nostopwords ? 1 : 0,
          entity: "article",
          // start: $scope.selectedRange ? $scope.selectedRange : '',
          // end: $scope.selectedRange ? parseInt($scope.selectedRange) + $scope.rangeStep : ''
        },
        function(result) {
          $scope.wordFrequency = result.words;
          $scope.total = result.total;
        },
      );

      var promise2 = WordDistribution.query(
        {
          search: $scope.search,
          timePeriod: "month",
          nonstrict: 1,
          sourceName: $scope.sourceName,
          entity: "article",
        },
        function(result) {
          $scope.wordDistribution = result;
          $scope.totalWords = _.reduce(
            result,
            function(memo, d) {
              return memo + d.count;
            },
            0,
          );
        },
      );

      var promise3 = WordsTimeRange.get(
        {
          search: $scope.search,
          nonstrict: 1,
          sourceName: $scope.sourceName,
        },
        function(result) {
          $scope.wordsTimeRange = result;
          $scope.start = result.from ? +new Date(result.from.created) : null;
          $scope.end = result.to ? +new Date(result.to.created) : null;
          // $scope.selectedRange = $scope.start;
        },
      );

      $q.all([promise1, promise2, promise3]).then(function() {
        console.log("All promises loaded");
        $state.go(
          "word_frequency_and_distribution",
          {
            search: $scope.search,
            sourceName: $scope.sourceName,
          },
          {
            notify: false,
          },
        );
      });
    },
  );

  $scope.$watch("skip", function(val) {
    if (!val) return;

    WordFrequency.get(
      {
        search: $scope.search,
        skip: $scope.skip,
        limit: limit,
        sortDir: $scope.sortDir,
        sourceName: $scope.sourceName,
        entity: "article",
        nostopwords: $scope.nostopwords ? 1 : 0,
      },
      function(result) {
        $scope.wordFrequency = $scope.wordFrequency.concat(result.words);
      },
    );
  });

  $scope.$watch("selectedRange", function(val) {
    console.log(val);
  });

  $(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      $scope.skip += limit;
      $scope.$apply();
    }
  });

  $scope.rangeSize = function() {
    if ($scope.wordsTimeRange.to) {
      var range =
        (new Date($scope.wordsTimeRange.to.created) -
          new Date($scope.wordsTimeRange.from.created)) /
        (60 * 60 * 24 * 30 * 1000);
      return Math.ceil(range);
    }
  };

  $scope.onDistributionBarClick = function(e, d) {
    WordInclusion.query(
      {
        search: $scope.search,
        start: d.date,
        end: d.date + appConstants.monthInMsec,
        nonstrict: 1,
        sourceName: $scope.sourceName,
      },
      function(result) {
        $scope.wordInclusion = result;
      },
    );
  };

  function goToWordInclusion() {
    var win = window.open(
      "http://localhost:3000/word/inclusion?nonstrict=1&search=" +
        $scope.search +
        "&start=" +
        d.date +
        "&end=" +
        (d.date + appConstants.monthInMsec),
      "_blank",
    );

    win.focus();
  }

  function goToMessageSearch() {
    var win = window.open(
      $state.href("message_search", {
        search: $scope.search,
        start: d.date,
        end: d.date + appConstants.monthInMsec,
      }),
      "_blank",
    );
  }

  $scope.onFrequencyChartBarClick = function(e, d) {
    $scope.search = "^" + d.word + "$";
    $(window).scrollTop(0);
    $scope.$apply();
  };

  function goToWordDistribution() {
    var win = window.open(
      $state.href("word_distribution", {
        word: d.word,
      }),
      "_blank",
    );
    win.focus();
  }
});
