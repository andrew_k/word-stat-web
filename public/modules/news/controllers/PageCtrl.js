var newsStat = angular.module("newsStat");

newsStat.controller("PageCtrl", function($scope, $state) {
  $scope.title =
    ($state.current && $state.current.data && $state.current.data.title) ||
    "Домашняя";
});
