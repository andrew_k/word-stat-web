var newsStat = angular.module("newsStat");

newsStat.controller("MessageSearchCtrl", function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  Message,
) {
  $rootScope.title = ["Поиск", '"' + $stateParams.search + '"'].join(" ");
  $scope.messages = [];
  $scope.search = $stateParams.search || "";
  $scope.total = 0;
  $scope.minifyMessages = $stateParams.minifyMessages === "true" || false;

  var limit = 40;
  $scope.skip = 0;
  $scope.start = $stateParams.start
    ? new Date(parseInt($stateParams.start))
    : null;
  $scope.end = $stateParams.end ? new Date(parseInt($stateParams.end)) : null;

  $scope.$watchGroup(["search", "start", "end"], function() {
    $scope.skip = 0;
    Message.get(
      {
        search: $scope.search,
        nonstrict: 1,
        sender: $stateParams.sender,
        start: +$scope.start,
        end: $scope.end === null ? "" : +$scope.end,
        skip: $scope.skip,
        limit: limit,
      },
      function(result) {
        $scope.messages = result.messages || [];
        $scope.total = result.total;

        $state.go(
          "message_search",
          {
            search: $scope.search,
            start: $scope.start === null ? "" : +$scope.start,
            end: $scope.end === null ? "" : +$scope.end,
          },
          {
            notify: false,
          },
        );
      },
    );
  });

  $scope.$watch("skip", function(val) {
    if (val == 0) return;
    Message.get(
      {
        search: $scope.search,
        nonstrict: 1,
        sender: $stateParams.sender,
        start: +$scope.start,
        end: +$scope.end,
        skip: $scope.skip,
        limit: limit,
      },
      function(result) {
        $scope.messages = $scope.messages.concat(result.messages || []);
        $scope.total = result.total;
      },
    );
  });

  $scope.$watch("minifyMessages", function(val) {
    $state.go(
      "message_search",
      {
        search: $scope.search,
        start: +$scope.start,
        end: +$scope.end,
        minifyMessages: val,
      },
      {
        notify: false,
      },
    );
  });

  $(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      $scope.skip += limit;
      $scope.$apply();
    }
  });
});
