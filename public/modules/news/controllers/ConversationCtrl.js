var newsStat = angular.module("newsStat");

newsStat.controller("ConversationCtrl", function(
  $rootScope,
  $scope,
  $state,
  $stateParams,
  Sender,
  Recipient,
  Message,
) {
  $rootScope.title = [
    "Беседа",
    $stateParams.sender,
    $stateParams.recipient,
  ].join(" ");

  $scope.messages = [];
  $scope.source = $stateParams.source;

  $scope.senders = [];
  $scope.recipients = [];

  $scope.personSender = {};
  $scope.personRecipient = {};

  $scope.minifyMessages = false;

  var limit = 40;
  $scope.skip = 0;

  Sender.query(
    {
      source: $stateParams.source,
    },
    function(result) {
      $scope.senders = result;
      if ($stateParams.sender) {
        $scope.personSender = _.find(result, function(m) {
          return m.userName == $stateParams.sender;
        });
      }
    },
  );

  var initialization = true;
  $scope.$watch("personSender", function(val) {
    if (_.isEmpty(val)) return;
    Recipient.query(
      {
        sender: val.userName,
        source: $stateParams.source,
      },
      function(result) {
        $scope.recipients = result;
        //should works only once, after page loaded
        if (initialization && $stateParams.recipient) {
          $scope.personRecipient = _.find(result, function(m) {
            return m.userName == $stateParams.recipient.replace("%2F", "/");
          });
          initialization = false;
        }
      },
    );

    $state.go(
      "conversation",
      {
        source: $stateParams.source,
        sender: val.userName,
      },
      {
        notify: false,
      },
    );
  });

  $scope.$watch("personRecipient", function(val) {
    if (_.isEmpty($scope.personSender) || _.isEmpty(val)) return;
    Message.query(
      {
        sender: $scope.personSender.userName,
        recipient: val.userName,
        source: $stateParams.source,
        limit: limit,
      },
      function(result) {
        $scope.messages = result.messages;

        $state.go(
          "conversation",
          {
            source: $stateParams.source,
            sender: $scope.personSender.userName,
            recipient: val.userName,
          },
          {
            notify: false,
          },
        );
      },
    );
  });

  $scope.$watch("skip", function(val) {
    if (val == 0) return;
    Message.get(
      {
        sender: $scope.personSender.userName,
        recipient: val.userName,
        source: $stateParams.source,
        skip: $scope.skip,
        limit: limit,
      },
      function(result) {
        $scope.messages = $scope.messages.concat(result.messages || []);
        $scope.total = result.total;
      },
    );
  });

  $(window).scroll(function() {
    if ($(window).scrollTop() + $(window).height() == $(document).height()) {
      $scope.skip += limit;
      $scope.$apply();
    }
  });
});
