var wordStat = angular.module("wordStat", [
  "ngResource",
  "ui.router",
  "wordStatServices",
]);

wordStat
  .constant("appConstants", {
    monthInMsec: 60 * 60 * 24 * 30 * 1000,
  })
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/word_frequency");
    $stateProvider
      // .state('home', {
      //     url: "/",
      //     data: {
      //         title: 'Домашняя'
      //     },
      //     templateUrl: '/templates/home.html',
      // })
      .state("statistics", {
        url: "/statistics",
        templateUrl: "/templates/statistics.html",
        controller: "StatisticsController",
      })
      .state("word_total", {
        url: "/?search&sourceName",
        templateUrl: "/templates/word_frequency_and_distribution.html",
        controller: "WordTotalCtrl",
      })
      .state("word_distribution", {
        url: "/word_distribution/:word/:relative",
        templateUrl: "/templates/word_distribution.html",
        controller: "WordDistributionCtrl",
      })
      .state("word_frequency", {
        url: "/word_frequency?search&sourceName",
        templateUrl: "/templates/word_frequency.html",
        controller: "WordFrequencyCtrl",
      })
      .state("conversation", {
        url: "/conversation/:source?sender&recipient",
        templateUrl: "/templates/conversation.html",
        controller: "ConversationCtrl",
      })
      .state("message_search", {
        url: "/message_search?search&start&end&sender&minifyMessages",
        templateUrl: "/templates/message_search.html",
        controller: "MessageSearchCtrl",
      })
      .state("word_inclusion", {
        url: "/word_inclusion?search&start&end",
        templateUrl: "/templates/word_inclusion.html",
        controller: "WordInclusionCtrl",
      })
      .state("article_distribution", {
        url: "/article_distribution",
        templateUrl: "/templates/article_distribution.html",
        controller: "ArticleDistributionCtrl",
      })
      .state("upload", {
        url: "/upload",
        templateUrl: "/templates/upload.html",
        controller: "UploadCtrl",
      });
  });

wordStat.run([
  "$location",
  "$rootScope",
  function($location, $rootScope) {
    $rootScope.$on("$stateChangeSuccess", function(event, current, previous) {
      if (current.data && current.data.title) {
        $rootScope.title = current.data.title;
      }
    });
  },
]);
