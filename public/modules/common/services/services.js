var wsCommonServices = angular.module("wsCommonServices", []);

wsCommonServices.factory("Sender", [
  "$resource",
  function($resource) {
    return $resource(
      "person/sender/list",
      {
        source: "@source",
      },
      {
        query: {
          method: "GET",
          isArray: true,
        },
      },
    );
  },
]);

wsCommonServices.factory("Recipient", [
  "$resource",
  function($resource) {
    return $resource(
      "person/recipient/list",
      {
        source: "@source",
        sender: "@sender",
      },
      {
        query: {
          method: "GET",
          isArray: true,
        },
      },
    );
  },
]);

wsCommonServices.factory("Message", [
  "$resource",
  function($resource) {
    return $resource(
      "message/list",
      {
        source: "@source",
        sender: "@sender",
        recipient: "@recipient",
        search: "@search",
        nonstrict: "@nonstrict",
        start: "@start",
        end: "@end",
        skip: "@skip",
        limit: "@limit",
        sort: "@sort",
        sortDir: "@sortDir",
      },
      {
        query: {
          method: "GET",
          isArray: false,
        },
      },
    );
  },
]);

wsCommonServices.factory("SearchMessage", [
  "$resource",
  function($resource) {
    return $resource(
      "search/message",
      {
        word: "@word",
        sender: "@sender",
        start: "@start",
        end: "@end",
        skip: "@skip",
        limit: "@limit",
      },
      {
        query: {
          method: "GET",
          isArray: false,
        },
      },
    );
  },
]);

wsCommonServices.factory("WordsTimeRange", [
  "$resource",
  function($resource) {
    return $resource(
      "word/words_time_range",
      {
        nonstrict: "@nonstrict",
        sourceName: "@sourceName",
      },
      {
        query: {
          method: "GET",
          isArray: false,
        },
      },
    );
  },
]);

wsCommonServices.factory("WordFrequency", [
  "$resource",
  function($resource) {
    return $resource(
      "word/frequency",
      {
        search: "@search",
        start: "@start",
        end: "@end",
        skip: "@skip",
        limit: "@limit",
        sort: "@sort",
        sortDir: "@sortDir",
        sourceName: "@sourceName",
        nostopwords: "@nostopwords",
        groupBy: "@groupBy",
        entity: "@entity",
      },
      {
        query: {
          method: "GET",
          isArray: false,
        },
      },
    );
  },
]);

wsCommonServices.factory("BigramFrequency", [
  "$resource",
  function($resource) {
    return $resource(
      "bigram/aggregated_by_stem",
      {
        search: "@search",
        skip: "@skip",
        limit: "@limit",
        sortDir: "@sortDir",
        sourceName: "@sourceName",
      },
      {
        query: {
          method: "GET",
          isArray: false,
        },
      },
    );
  },
]);

wsCommonServices.factory("WordDistribution", [
  "$resource",
  function($resource) {
    return $resource(
      "word/distribution",
      {
        word: "@word",
        timePeriod: "@timePeriod",
        nonstrict: "@nonstrict",
        sourceName: "@sourceName",
        entity: "@entity",
      },
      {
        query: {
          method: "GET",
          isArray: true,
        },
      },
    );
  },
]);

wsCommonServices.factory("WordInclusion", [
  "$resource",
  function($resource) {
    return $resource(
      "word/inclusion",
      {
        search: "@word",
        start: "@start",
        end: "@end",
        nonstrict: "@skip",
        sourceName: "@sourceName",
      },
      {
        query: {
          method: "GET",
          isArray: true,
        },
      },
    );
  },
]);

wsCommonServices.factory("ArticleDistribution", [
  "$resource",
  function($resource) {
    return $resource(
      "article/distribution",
      {},
      {
        query: {
          method: "GET",
          isArray: true,
        },
      },
    );
  },
]);

wsCommonServices.factory("TextEntity", [
  "$resource",
  function($resource) {
    return $resource(
      "text_entity",
      {},
      {
        save: {
          method: "POST",
          isArray: false,
        },
      },
    );
  },
]);

wsCommonServices.factory("TextEntityTitles", [
  "$resource",
  function($resource) {
    return $resource(
      "text_entity/titles",
      {},
      {
        save: {
          method: "GET",
          isArray: true,
        },
      },
    );
  },
]);
