var wsCommonDirectives = angular.module("wsCommonDirectives");

wsCommonDirectives.directive("searchMessageComponent", function(
  $state,
  Message,
) {
  return {
    restrict: "E",
    scope: {
      messages: "=",
      minify: "=",
    },
    templateUrl: "javascripts/directives/templates/searchMessageComponent.html",

    link: function(scope, iElement, iAttrs) {},
  };
});
