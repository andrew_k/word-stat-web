var wsCommonDirectives = angular.module("wsCommonDirectives");

wsCommonDirectives.directive("detailFrequencyChart", function(
  $state,
  appConstants,
) {
  return {
    restrict: "E",
    scope: {
      wordsData: "=",
      onBarClick: "&",
    },
    template:
      '<svg class="detail-frequency-chart"><g class="plot">' +
      '<g class="bars-container"></g>' +
      "</g></svg>",

    link: function(scope, iElement, iAttrs) {
      var width =
          parseInt(iAttrs.chartWidth) ||
          $(iElement[0])
            .parent()
            .width(),
        height = parseInt(iAttrs.chartHeight) || 500,
        margin = {
          top: 20,
          right: 20,
          bottom: 60,
          left: 10,
        },
        plotWidth = width - (margin.right + margin.left),
        plotHeight = height - (margin.top + margin.bottom),
        barHeight = 30;

      var chart = d3
        .select(iElement[0])
        .select(".detail-frequency-chart")
        .attr("width", plotWidth);

      var plot = chart
        .select(".plot")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      scope.$watch("wordsData", function(val) {
        if (!val) return;

        chart.attr(
          "height",
          margin.top + margin.bottom + val.length * barHeight,
        );

        var x = d3.scale
          .linear()
          .domain([
            0,
            d3.max(val, function(w) {
              return w.count;
            }),
          ])
          .range([0, plotWidth]);

        var y = d3.scale
          .ordinal()
          .domain(_.pluck(val, "word"))
          .rangeBands([0, val.length * barHeight]);

        var yAxis = d3.svg
          .axis()
          .scale(y)
          .orient("left");

        var bar = plot.selectAll(".bar").data(val, function(d, i) {
          return i;
        });

        bar.exit().remove();

        var newBar = bar
          .enter()
          .append("g")
          .attr("class", "bar")
          .attr("transform", "translate(0,0)")
          .on("click", function(d) {
            if (scope.onBarClick)
              scope.onBarClick({
                event: d3.event,
                d: d,
              });
          });

        newBar
          .append("rect")
          .attr("class", "rect-background")
          .attr("width", plotWidth)
          .attr("height", barHeight - 1);

        newBar
          .append("rect")
          .attr("class", "rect-data")
          .attr("width", 0)
          .attr("height", barHeight - 1);

        var wordData = newBar.append("text").attr("class", "word-data");
        wordData.append("tspan").attr("class", "word");
        wordData.append("tspan").attr("class", "word-count");
        wordData.append("tspan").attr("class", "stems");

        plot.selectAll(".bar").attr("transform", function(d, i) {
          return "translate(0," + y(d.word) + ")";
        });

        var rect = plot.selectAll(".bar").select(".rect-data");
        rect.transition().attr("width", function(d) {
          return x(d.count);
        });

        var wordDataSel = plot
          .selectAll(".bar")
          .select(".word-data")
          .attr("x", 10)
          .attr("y", barHeight / 2)
          .attr("dy", ".35em");

        wordDataSel.select(".word").text(function(d) {
          return d.word;
        });

        wordDataSel
          .select(".word-count")
          .attr("dx", 10)
          .text(function(d) {
            return d.count;
          });

        wordDataSel
          .select(".stems")
          .attr("dx", 10)
          .text(function(d) {
            var words;
            if (d.words.length === 1 && d.words[0] === d.word) {
              words = "";
            } else {
              var maxWords = 6;
              if (d.words.length > maxWords) {
                words = " [" + d.words.slice(0, maxWords).join(", ");
                words += "...] ещё " + (d.words.length - maxWords);
              } else {
                words = " [" + d.words.join(", ") + "]";
              }
            }
            return words;
          });
      });
    },
  };
});
