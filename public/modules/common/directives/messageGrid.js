var wsCommonDirectives = angular.module("wsCommonDirectives");

wsCommonDirectives.directive("messageGrid", function($state) {
  return {
    restrict: "E",
    // require: '^messages',
    scope: {
      messages: "=",
      minify: "=",
    },
    templateUrl: "javascripts/directives/templates/messageGrid.html",

    link: function(scope, iElement, iAttrs) {},
  };
});
