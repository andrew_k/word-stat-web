var wsCommonDirectives = angular.module("wsCommonDirectives");

wsCommonDirectives.directive("frequencyChart", function($state, appConstants) {
  return {
    restrict: "E",
    scope: {
      wordsData: "=",
      onBarClick: "&",
    },
    template:
      '<svg class="frequency-chart"><g class="plot">' +
      '<g class="bars-container"></g>' +
      "</g></svg>",

    link: function(scope, iElement, iAttrs) {
      var width =
          parseInt(iAttrs.chartWidth) ||
          $(iElement[0])
            .parent()
            .width(),
        height = parseInt(iAttrs.chartHeight) || 500,
        margin = {
          top: 20,
          right: 20,
          bottom: 60,
          left: 10,
        },
        plotWidth = width - (margin.right + margin.left),
        plotHeight = height - (margin.top + margin.bottom),
        barHeight = 20;

      var chart = d3
        .select(iElement[0])
        .select(".frequency-chart")
        .attr("width", plotWidth);

      var plot = chart
        .select(".plot")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      scope.$watch("wordsData", function(val) {
        if (!val) return;

        chart.attr(
          "height",
          margin.top + margin.bottom + val.length * barHeight,
        );

        var x = d3.scale
          .linear()
          .domain([
            0,
            d3.max(val, function(w) {
              return w.count;
            }),
          ])
          .range([0, plotWidth]);

        var y = d3.scale
          .ordinal()
          .domain(_.pluck(val, "word"))
          .rangeBands([0, val.length * barHeight]);

        var yAxis = d3.svg
          .axis()
          .scale(y)
          .orient("left");

        var bar = plot.selectAll(".bar").data(val, function(d, i) {
          return i;
          // return d.word;
        });

        bar.exit().remove();

        var newBar = bar
          .enter()
          .append("g")
          .attr("class", "bar")
          .attr("transform", "translate(0,0)")
          .on("click", function(d) {
            if (scope.onBarClick)
              scope.onBarClick({
                event: d3.event,
                d: d,
              });
          });

        newBar.append("rect").attr("width", 0);
        newBar.append("text");

        plot.selectAll(".bar").attr("transform", function(d, i) {
          return "translate(0," + y(d.word) + ")";
        });

        var rect = plot.selectAll(".bar").select("rect");
        rect
          .transition()
          .attr("width", function(d) {
            return x(d.count);
          })
          .attr("height", barHeight - 1);

        plot
          .selectAll(".bar")
          .select("text")
          .attr("x", 5)
          .attr("y", barHeight / 2)
          .attr("dy", ".35em")
          .text(function(d) {
            var words;
            if (d.words.length === 1 && d.words[0] === d.word) {
              words = "";
            } else {
              words = " [" + d.words.join(", ") + "]";
            }
            return d.word + " " + d.count + words;
          });
      });
    },
  };
});
