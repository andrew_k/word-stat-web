var wsCommonDirectives = angular.module("wsCommonDirectives", []);

wsCommonDirectives.constant("appConstants", {
  monthInMsec: 60 * 60 * 24 * 30 * 1000,
});
