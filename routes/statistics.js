var express = require("express");
var router = express.Router();
var async = require("async");
var _ = require("underscore");

var models = require("../src/models");
var Message = models.Message;
var Word = models.Word;

router.get("/sentences", function(req, res) {
  // console.log($ch);
  // res.json({});

  // return;

  Message.find(
    {
      userName: "andrew_k49",
      messageText: /http/,
    },
    { messageText: 1, created: 1 },
  ).exec(function(err, result) {
    // return res.json(result);
    var prepared = _.map(result, function(message) {
      var tags = findTags(message.messageText);
      var hrefs = findHref(tags.outText);
      return {
        messageText: hrefs.outText,
        tags: tags.tags,
        hrefs: hrefs.href,
        created: message.created,
      };
    });

    var wc = wordsCount(prepared);
    var wca = _.map(wc, function(count, key) {
      return {
        word: key,
        count: count,
      };
    });

    res.json(
      wca.sort(function(a, b) {
        return b.count - a.count;
      }),
    );
    return res.json(result);
  });
});

router.get("/parse_and_save_words", function(req, res) {
  var sourceName = "qip";

  Message.find(
    {
      userName: "andrew_k49",
      sourceName: sourceName,
    },
    { messageText: 1, created: 1 },
  ).exec(function(err, result) {
    // return res.json(result);
    var prepared = _.map(result, function(message) {
      var tags = findTags(message.messageText);
      var hrefs = findHref(tags.outText);
      return {
        messageText: hrefs.outText,
        tags: tags.tags,
        hrefs: hrefs.href,
        created: message.created,
      };
    });

    var words = [];
    _.each(prepared, function(m) {
      var messageWords = _.map(getWords(m.messageText), function(w) {
        return {
          word: w,
          created: m.created,
          sourceName: sourceName,
        };
      });
      words = words.concat(messageWords);
    });
    // res.json(words);

    // return;
    async.eachSeries(
      words,
      function(w, callback, i) {
        new Word(w).save(callback);
      },
      function(err) {
        if (err) {
          res.json({
            error: err,
            count: words.length,
          });
        } else {
          res.json({
            status: "finished",
            count: words.length,
          });
        }
      },
    );
  });
});

// router.get('/words_total', function(req, res) {
// 	var mongoQuery = [
// 		{$group: {_id: '$word', count: {$sum: 1}}},
// 		{$project: {_id: 0, word: '$_id', count: 1}},
// 	];

// 	var sort = req.query.sort == 'created' ? 'created' : 'count';
// 	var sortDir = req.query.sortDir === '-1' ? -1 : 1;
// 	if (sort == 'created') {
// 		mongoQuery.push({$sort: {created: sortDir}});
// 	} else {
// 		mongoQuery.push({$sort: {count: sortDir}});
// 	}
// 	var skip = parseInt(req.query.skip) || 0;
// 	mongoQuery.push({$skip: skip});

// 	var limit = parseInt(req.query.limit) || 100;
// 	mongoQuery.push({$limit: limit});

// 	var match = {
// 		$match: {}
// 	};
// 	if(req.query.search) {
// 		match['$match']['word'] = new RegExp(req.query.search);
// 	}
// 	if(req.query.source) {
// 		match['$match']['sourceName'] = req.query.source;
// 	}
// 	if(req.query.start || req.query.end) {
// 		match['$match']['created'] = {};
// 	}
// 	if(req.query.start) {
// 		match['$match']['created']['$gte'] = new Date(parseInt(req.query.start))
// 	}
// 	if(req.query.end) {
// 		match['$match']['created']['$lte'] = new Date(parseInt(req.query.end))
// 	}
// 	if(!_.isEmpty(match['$match'])) {
// 		mongoQuery.unshift(match);
// 	}

// 	async.parallel({
// 		words: function(cb) {
// 			Word.aggregate(mongoQuery, cb);
// 		},
// 		total: function(cb) {
// 			var totalQuery = [
// 				{$group: {_id: '$word', count: {$sum: 1}}},
// 				{$group: { _id: '',count: {$sum:1}}}
// 			];
// 			if(!_.isEmpty(match['$match'])) {
// 				totalQuery.unshift(match);
// 			}
// 			Word.aggregate(totalQuery, function(err, total){
// 				cb(err, (total && total[0] && total[0]['count']) || 0);
// 			});
// 		}
// 	}, function(err, result) {
// 		if(err) {
// 			res.json({
// 				error: err
// 			});
// 		} else {
// 			result['meta'] = {
// 				skip: skip,
// 				limit: limit
// 			};
// 			res.json(result);
// 		}
// 	});

// });

// var validPeriods = ['month'];
// router.get('/word_frequency', function(req, res) {
// 	if(false /* validPeriods.indexOf(req.query.timePeriod) === -1*/) {
// 		return res.send('empty request');
// 	} else {
// 		var mongoQuery = [
// 			{$group: {
// 		        _id: {year: '$year', month: '$month'},
// 		        count: {$sum: 1}
// 		     }
// 		    },
// 		    {$project: {year: '$_id.year', month: '$_id.month', count: 1, _id: 0}},
// 		    {$sort: {year: 1, month: 1}}
// 		];
// 		if(req.query.timePeriod == 'month') {
// 			mongoQuery.unshift({$project: {
// 		        month : {$month : "$created"},
// 		        year : {$year : "$created"}
// 		        }
// 		    });
// 		}
// 		if(req.query.search) {
// 			mongoQuery.unshift({
// 				$match: {
// 					word: req.query.nonstrict === '1' ? new RegExp(req.query.search): req.query.search
// 				}
// 			});
// 		}

// 		Word.aggregate(mongoQuery, function(err, result){
// 			res.json(result);
// 		});
// 	}

// });

// router.get('/words_time_range', function(req, res) {
// 	var mongoQuery = {};
// 	if(req.query.search) {
// 		mongoQuery['word'] = req.query.nonstrict === '1' ? new RegExp(req.query.search) : req.query.search;
// 	}
// 	async.parallel({
// 		from: function(callback) {
// 			Word.find(mongoQuery, {_id: 0, __v: 0}).sort({created: 1}).limit(1).exec(callback);
// 		},
// 		to: function(callback) {
// 			Word.find(mongoQuery, {_id: 0, __v: 0}).sort({created: -1}).limit(1).exec(callback);
// 		}
// 	}, function(err, result) {
// 		if(err) {
// 			res.send(err);
// 		} else {
// 			res.json({
// 				from: result.from[0],
// 				to: result.to[0]
// 			});
// 		}
// 	});
// });

var skypeTags = ["a", ""];
var tagRegexp = new RegExp("<(.+).*>(.+)</\\1>");

function findTags(text) {
  var tagsArray = [];
  var outText = text;

  var tags = tagRegexp.exec(outText);
  while (tags) {
    tagsArray.push(tags[0]);
    outText = outText.replace(tags[0], tags[2]);
    tags = tagRegexp.exec(outText);
  }
  return {
    outText: outText,
    tags: tagsArray,
  };
}

function findHref(text) {
  var hrefRegexp = /(http:\/\/.+?)(\s|$)/;

  var hrefArray = [];
  var outText = text;

  var href = hrefRegexp.exec(outText);
  // console.log(outText, href);
  while (href) {
    hrefArray.push(href[0]);
    outText = outText.replace(href[0], "");
    href = hrefRegexp.exec(outText);
  }
  return {
    outText: outText,
    href: hrefArray,
  };
}

function wordsCount(messages) {
  var words = {};

  _.each(messages, function(m) {
    var m_words = getWords(m.messageText);
    _.each(m_words, function(w) {
      if (!words[w]) {
        words[w] = 1;
      } else {
        words[w]++;
      }
    });
  });

  return words;
}

function getWords(message) {
  if (!message) return [];
  return message.split(/[,.\s!?]/).filter(function(w) {
    return w !== "";
  });
}

module.exports = router;
