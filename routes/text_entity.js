var express = require("express");
var router = express.Router();
var async = require("async");
var _ = require("underscore");
var nlp = require("../nlp");

var models = require("../src/models");
var Message = models.Message;
var Word = models.Word;
var TextEntity = models.TextEntity;

router.post("/", function(req, res) {
  console.log("req.body", req.body);
  if (!req.body.title || !req.body.text) return res.send(400, "Bad fields");

  new TextEntity({
    title: req.body.title,
    text: req.body.text,
  }).save(function(err, item) {
    if (err) {
      res.json(err);
    } else {
      res.json({
        _id: item._id,
        title: item.title,
      });
    }
  });
});

router.get("/analyze", function(req, res) {
  TextEntity.find({
    processed: {
      $ne: true,
    },
  }).exec(function(err, ents) {
    var tokenCount = {};
    async.eachSeries(
      ents,
      function(ent, entCb) {
        var tokens = nlp.tokenize(ent.text);
        tokenCount[ent.title] = tokens.length;
        async.eachSeries(
          tokens,
          function(tok, tokCb) {
            new Word({
              word: tok,
              sourceName: ent.title,
            }).save(tokCb);
          },
          function(err) {
            if (err) {
              entCb(err);
            } else {
              TextEntity.update(
                {
                  _id: ent.id,
                },
                {
                  $set: {
                    processed: true,
                  },
                },
              ).exec(entCb);
            }
          },
        );
      },
      function(err) {
        if (err) {
          res.json(err);
        } else {
          res.json(tokenCount);
        }
      },
    );
  });
});

router.get("/delete_words", function(req, res) {
  TextEntity.find({}).exec(function(err, ents) {
    var tokenCount = {};
    var entTitles = _.map(ents, function(ent) {
      return ent.title;
    });
    Word.remove({
      sourceName: {
        $in: entTitles,
      },
    }).exec(function(err, result) {
      if (err) {
        res.json(err);
      } else {
        TextEntity.update(
          {
            title: {
              $in: entTitles,
            },
          },
          {
            processed: false,
          },
          {
            multi: true,
          },
        ).exec(function(err, result) {
          if (err) {
            res.json(err);
          } else {
            res.json(result);
          }
        });
      }
    });
  });
});

router.get("/titles", function(req, res) {
  TextEntity.find().exec(function(err, result) {
    if (err) {
      res.json(err);
    } else {
      var sourceNames = _.map(result, function(ent) {
        return ent.title;
      });
      res.json(sourceNames);
    }
  });
});

module.exports = router;
