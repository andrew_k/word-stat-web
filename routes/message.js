var express = require("express");
var router = express.Router();
var async = require("async");
var models = require("../src/models");
var Message = models.Message;

/* GET users listing. */
router.get("/list", function(req, res) {
  var skip = req.query.skip || 0,
    limit = req.query.limit || 40;

  var mongoQuery = {};
  if (req.query.search) {
    mongoQuery["messageText"] =
      req.query.nonstrict === "1"
        ? new RegExp(req.query.search, "i")
        : req.query.search;
  }
  if (req.query.start || req.query.end) {
    mongoQuery["created"] = {};
  }
  if (req.query.start) {
    mongoQuery["created"]["$gte"] = new Date(parseInt(req.query.start));
  }
  if (req.query.end) {
    mongoQuery["created"]["$lte"] = new Date(parseInt(req.query.end));
  }
  if (req.query.sourceName) {
    mongoQuery["sourceName"] = req.query.source;
  }
  if (req.query.sender) {
    mongoQuery["userName"] = req.query.sender;
  }
  if (req.query.recipient) {
    mongoQuery["recipient"] = req.query.recipient;
  }
  // if (req.query.sender || req.query.recipient) {
  // 	mongoQuery['$or'] = [{
  // 		$and: [{
  // 			userName: req.query.sender
  // 		}, {
  // 			recipient: req.query.recipient
  // 		}]
  // 	}, {
  // 		$and: [{
  // 			userName: req.query.recipient
  // 		}, {
  // 			recipient: req.query.sender
  // 		}]
  // 	}];
  // }
  console.log(mongoQuery);
  async.parallel(
    {
      messages: function(cb) {
        Message.find(mongoQuery, {
          data: 0,
        })
          .sort({
            created: req.query.sortDir === "-1" ? -1 : 1,
          })
          .skip(skip)
          .limit(limit)
          .exec(function(err, result) {
            cb(err, result);
          });
      },
      total: function(cb) {
        Message.count(mongoQuery).exec(function(err, result) {
          cb(err, result);
        });
      },
    },
    function(err, result) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        result["meta"] = {
          skip: skip,
          limit: limit,
        };
        res.json(result);
      }
    },
  );
});

module.exports = router;
