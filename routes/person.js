var express = require("express");
var router = express.Router();
var _ = require("underscore");

var models = require("../src/models");
var Message = models.Message;

/* GET users listing. */
router.get("/sender/list", function(req, res) {
  var mongoQuery = [
    { $group: { _id: { userName: "$userName" }, count: { $sum: 1 } } },
    { $sort: { count: -1 } },
    {
      $project: {
        _id: 0,
        // displayName: '$_id.displayName',
        userName: "$_id.userName",
        count: 1,
      },
    },
  ];
  if (req.query.source) {
    mongoQuery.unshift({ $match: { sourceName: req.query.source } });
  }
  Message.aggregate(mongoQuery, function(err, result) {
    res.json(result);
  });
});

router.get("/recipient/list", function(req, res) {
  var mongoQuery = [
    { $group: { _id: { recipient: "$recipient" }, count: { $sum: 1 } } },
    { $sort: { count: -1 } },
    {
      $project: {
        _id: 0,
        displayName: "$_id.recipient",
        userName: "$_id.recipient",
        count: 1,
      },
    },
  ];
  if (req.query.sender) {
    mongoQuery.unshift({
      $match: { userName: req.query.sender },
    });
  }
  if (req.query.source) {
    mongoQuery.unshift({ $match: { sourceName: req.query.source } });
  }
  Message.aggregate(mongoQuery, function(err, result) {
    res.json(result);
  });
});

module.exports = router;
