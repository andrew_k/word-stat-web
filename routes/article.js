var express = require("express");
var router = express.Router();
var async = require("async");
var _ = require("underscore");

var models = require("../src/models");
var Message = models.Message;
var Word = models.Word;
var Article = models.Article;

router.get("/distribution", function(req, res) {
  Article.aggregate([
    // {
    // 	$match: {
    // 		created: {
    // 			$ne: null
    // 		}
    // 	}
    // },
    {
      $project: {
        created: {
          $add: ["$created", 1000 * 60 * 60 * 2],
        },
        // createdRaw: 1
      },
    },
    {
      $project: {
        year: {
          $year: "$created",
        },
        month: {
          $month: "$created",
        },
        // created: 1,
        // createdRaw: 1
      },
    },
    // {
    // 	$match: {
    // 		year: 2013
    // 	}
    // }
    {
      $group: {
        _id: {
          year: "$year",
          month: "$month",
        },
        count: {
          $sum: 1,
        },
      },
    },
    {
      $sort: {
        "_id.year": 1,
        "_id.month": 1,
      },
    },
    {
      $project: {
        year: "$_id.year",
        month: "$_id.month",
        count: 1,
        _id: 0,
      },
    },
  ]).exec(function(err, result) {
    if (err) {
      res.send(err);
    } else {
      res.json(result);
    }
  });
});

module.exports = router;
