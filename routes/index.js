var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res) {
  res.render("index", { title: "Home" });
});
router.get("/book", function(req, res) {
  res.render("book", { title: "Book" });
});
router.get("/news", function(req, res) {
  res.render("news", { title: "News" });
});
module.exports = router;
