var express = require("express");
var router = express.Router();
var async = require("async");
var _ = require("underscore");

var models = require("../src/models");
var Message = models.Message;
var Word = models.Word;
var AgregatedWord = models.AgregatedWord;
var Article = models.Article;
var TextEntity = models.TextEntity;
var fs = require("fs");

var stopWords = fs
  .readFileSync("./data/russian")
  .toString()
  .split("\n");

router.get("/aggregated_by_stem", function(req, res) {
  AgregatedWord.aggregate(
    {
      $match: {
        sourceName: new RegExp(req.query.sourceName || ""),
        stem: {
          $exists: true,
        },
      },
    },
    {
      $group: {
        _id: "$stem",
        count: {
          $sum: 1,
        },
        total: {
          $sum: "$count",
        },
        words: {
          $addToSet: "$word",
        },
      },
    },
    {
      $sort: {
        count: -1,
      },
    },
    {
      $project: {
        _id: 0,
        лемма: "$_id",
        слов: "$total",
        формы: "$words",
      },
    },
  ).exec(function(err, result) {
    if (err) {
      res.send(err);
    } else {
      res.json(result);
    }
  });
});

router.get("/source_names", function(req, res) {
  // Word.aggregate({
  // 	$group: {
  // 		_id: '$sourceName',
  // 		count: {$sum: 1}
  // 	}
  // }).exec(function(err, result) {
  // 	res.json(result);
  // });
  TextEntity.find().exec(function(err, result) {
    if (err) {
      res.json(err);
    } else {
      var sourceNames = _.map(result, function(ent) {
        return ent.title;
      });
      res.json(sourceNames);
    }
  });
});

router.get("/frequency_no_date", function(req, res) {
  var mongoQuery = {
    sourceName: req.query.sourceName,
  };

  if (req.query.search) {
    mongoQuery["word"] = new RegExp(req.query.search);
  }

  var skip = parseInt(req.query.skip) || 0;
  var limit = parseInt(req.query.limit) || 100;

  async.parallel(
    {
      words: function(cb) {
        AgregatedWord.find(mongoQuery)
          .sort({
            count: req.query.sortDir === "-1" ? -1 : 1,
          })
          .skip(skip)
          .limit(limit)
          .exec(cb);
      },
      uniq: function(cb) {
        AgregatedWord.count(mongoQuery, function(err, total) {
          cb(err, total);
        });
      },
      total: function(cb) {
        Word.count(mongoQuery).exec(function(err, items) {
          cb(err, items);
        });
      },
    },
    function(err, result) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        result["meta"] = {
          skip: skip,
          limit: limit,
        };
        res.json(result);
      }
    },
  );
});
router.get("/frequency", function(req, res) {
  if (!req.query.sourceName) {
    return res.json([]);
  }

  var wordModel;
  if (req.query.entity === "article") {
    wordModel = models.WordArticle;
  } else {
    wordModel = models.Word;
  }

  var mongoQuery = [
    // {$match: {sourceName: 'korr'}},
    {
      $group: {
        _id: req.query.groupBy == "stem" ? "$stem" : "$word",
        count: {
          $sum: 1,
        },
        words: {
          $addToSet: "$word",
        },
      },
    },
    {
      $project: {
        _id: 0,
        word: "$_id",
        count: 1,
        words: 1,
      },
    },
  ];

  var sort = req.query.sort == "created" ? "created" : "count";
  var sortDir = req.query.sortDir === "-1" ? -1 : 1;
  if (sort == "created") {
    mongoQuery.push({
      $sort: {
        created: sortDir,
      },
    });
  } else {
    mongoQuery.push({
      $sort: {
        count: sortDir,
      },
    });
  }
  var skip = parseInt(req.query.skip) || 0;
  mongoQuery.push({
    $skip: skip,
  });

  var limit = parseInt(req.query.limit) || 100;
  mongoQuery.push({
    $limit: limit,
  });

  var match = {
    $match: {},
  };
  if (req.query.search) {
    match["$match"]["word"] = new RegExp(req.query.search);
  }
  if (req.query.sourceName) {
    match["$match"]["sourceName"] = req.query.sourceName;
  }
  if (req.query.start || req.query.end) {
    match["$match"]["created"] = {};
  }
  if (req.query.start) {
    match["$match"]["created"]["$gte"] = new Date(parseInt(req.query.start));
  }
  if (req.query.end) {
    match["$match"]["created"]["$lte"] = new Date(parseInt(req.query.end));
  }
  if (!_.isEmpty(match["$match"])) {
    mongoQuery.unshift(match);
  }
  if (req.query.nostopwords === "1") {
    mongoQuery.unshift({
      $match: {
        word: {
          $nin: stopWords,
        },
      },
    });
  }

  async.parallel(
    {
      words: function(cb) {
        wordModel.aggregate(mongoQuery, cb);
      },
      uniq: function(cb) {
        var totalQuery = [
          {
            $group: {
              _id: "$word",
              count: {
                $sum: 1,
              },
            },
          },
          {
            $group: {
              _id: "",
              count: {
                $sum: 1,
              },
            },
          },
        ];
        if (!_.isEmpty(match["$match"])) {
          totalQuery.unshift(match);
        }
        wordModel.aggregate(totalQuery, function(err, total) {
          cb(err, (total && total[0] && total[0]["count"]) || 0);
        });
      },
      total: function(cb) {
        wordModel.count(match["$match"]).exec(function(err, items) {
          cb(err, items);
        });
      },
    },
    function(err, result) {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        result["meta"] = {
          skip: skip,
          limit: limit,
        };
        res.json(result);
      }
    },
  );
});

var validPeriods = ["month"];
router.get("/distribution", function(req, res) {
  if (!req.query.sourceName) {
    return res.json([]);
  } else {
    var wordModel;
    if (req.query.entity === "article") {
      wordModel = models.WordArticle;
    } else {
      wordModel = models.Word;
    }

    var mongoQuery = [
      {
        $group: {
          _id: {
            year: "$year",
            month: "$month",
          },
          count: {
            $sum: 1,
          },
        },
      },
      {
        $project: {
          year: "$_id.year",
          month: "$_id.month",
          count: 1,
          _id: 0,
        },
      },
      {
        $sort: {
          year: 1,
          month: 1,
        },
      },
    ];
    if (req.query.timePeriod == "month") {
      mongoQuery.unshift({
        $project: {
          month: {
            $month: "$created",
          },
          year: {
            $year: "$created",
          },
        },
      });
    }
    var match = {};
    if (req.query.search) {
      match["word"] =
        req.query.nonstrict === "1"
          ? new RegExp(req.query.search)
          : req.query.search;
    }
    if (req.query.sourceName) {
      match["sourceName"] = req.query.sourceName;
    }
    if (!_.isEmpty(match)) {
      mongoQuery.unshift({
        $match: match,
      });
    }

    console.log("distribution", JSON.stringify(mongoQuery, null, 2));
    wordModel.aggregate(mongoQuery, function(err, result) {
      if (err) {
        res.send(500, err);
      } else {
        res.json(result);
      }
    });
  }
});

router.get("/words_time_range", function(req, res) {
  var mongoQuery = {};
  if (req.query.sourceName) {
    mongoQuery["sourceName"] = req.query.sourceName;
  }
  if (req.query.search) {
    mongoQuery["word"] =
      req.query.nonstrict === "1"
        ? new RegExp(req.query.search)
        : req.query.search;
  }
  async.parallel(
    {
      from: function(callback) {
        Word.find(mongoQuery, {
          _id: 0,
          __v: 0,
        })
          .sort({
            created: 1,
          })
          .limit(1)
          .exec(callback);
      },
      to: function(callback) {
        Word.find(mongoQuery, {
          _id: 0,
          __v: 0,
        })
          .sort({
            created: -1,
          })
          .limit(1)
          .exec(callback);
      },
    },
    function(err, result) {
      if (err) {
        res.send(err);
      } else {
        res.json({
          from: result.from[0],
          to: result.to[0],
        });
      }
    },
  );
});

router.get("/inclusion", function(req, res) {
  if (!req.query.search) {
    return res.json([]);
  }

  var mongoQuery = {};

  var SourceModel;
  if (req.query.sourceName == "korr") {
    SourceModel = Article;
  } else {
    SourceModel = Message;
    mongoQuery["sourceName"] = req.query.sourceName;
  }

  if (req.query.search) {
    mongoQuery["word"] =
      req.query.nonstrict === "1"
        ? new RegExp(req.query.search.toLowerCase(), "i")
        : req.query.search;
  }
  if (req.query.start || req.query.end) {
    mongoQuery["created"] = {};
  }
  if (req.query.start) {
    mongoQuery["created"]["$gte"] = new Date(parseInt(req.query.start));
  }
  if (req.query.end) {
    mongoQuery["created"]["$lte"] = new Date(parseInt(req.query.end));
  }
  console.log(mongoQuery);
  async.waterfall(
    [
      function(cb) {
        models.WordArticle.find(mongoQuery).exec(function(err, words) {
          cb(err, words);
        });
      },
      function(words, cb) {
        // console.log(words, cb);
        var sourceIds = _.map(words, function(w) {
          return w.sourceId;
        });
        console.log({
          _id: {
            $in: sourceIds,
          },
        });
        SourceModel.find({
          _id: {
            $in: sourceIds,
          },
        }).exec(cb);
      },
    ],
    function(err, result) {
      if (err) {
        res.send(err);
      } else {
        res.json(result);
      }
    },
  );
});

var skypeTags = ["a", ""];
var tagRegexp = new RegExp("<(.+).*>(.+)</\\1>");

function findTags(text) {
  var tagsArray = [];
  var outText = text;

  var tags = tagRegexp.exec(outText);
  while (tags) {
    tagsArray.push(tags[0]);
    outText = outText.replace(tags[0], tags[2]);
    tags = tagRegexp.exec(outText);
  }
  return {
    outText: outText,
    tags: tagsArray,
  };
}

function findHref(text) {
  var hrefRegexp = /(http:\/\/.+?)(\s|$)/;

  var hrefArray = [];
  var outText = text;

  var href = hrefRegexp.exec(outText);
  // console.log(outText, href);
  while (href) {
    hrefArray.push(href[0]);
    outText = outText.replace(href[0], "");
    href = hrefRegexp.exec(outText);
  }
  return {
    outText: outText,
    href: hrefArray,
  };
}

function wordsCount(messages) {
  var words = {};

  _.each(messages, function(m) {
    var m_words = getWords(m.messageText);
    _.each(m_words, function(w) {
      if (!words[w]) {
        words[w] = 1;
      } else {
        words[w]++;
      }
    });
  });

  return words;
}

function getWords(message) {
  if (!message) return [];
  return message.split(/[,.\s!?]/).filter(function(w) {
    return w !== "";
  });
}

module.exports = router;
