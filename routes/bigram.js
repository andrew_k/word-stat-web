var express = require("express");
var router = express.Router();
var async = require("async");
var _ = require("underscore");

var models = require("../src/models");
var Message = models.Message;
var Word = models.Word;
var AgregatedWord = models.AgregatedWord;
var Article = models.Article;
var TextEntity = models.TextEntity;
var fs = require("fs");

var stopWords = fs
  .readFileSync("./data/russian")
  .toString()
  .split("\n");

router.get("/aggregated_by_stem", function(req, res) {
  models.Bigram.aggregate(
    {
      $match: {
        sourceName: req.query.sourceName,
      },
    },
    {
      $group: {
        _id: {
          stem1: "$stem1",
          stem2: "$stem2",
        },
        count: {
          $sum: 1,
        },
        words1: {
          $addToSet: "$word1",
        },
        words2: {
          $addToSet: "$word2",
        },
      },
    },
    {
      $sort: {
        count: req.query.sortDir === "-1" ? -1 : 1,
      },
    },
    {
      $skip: parseInt(req.query.skip) || 0,
    },
    {
      $limit: parseInt(req.query.limit) || 100,
    },
  ).exec(function(err, result) {
    console.log(err, result);
    if (err) {
      res.send(500, err);
    } else {
      res.json({
        bigrams: result,
      });
    }
  });
});

module.exports = router;
