module.exports = function(grunt) {
  grunt.loadNpmTasks("grunt-contrib-compass");

  // Project configuration.
  grunt.initConfig({
    compass: {
      dev: {
        options: {
          sassDir: "public/modules/common/sass",
          cssDir: "public_dist/stylesheets",
          specify: ["**/style.scss"],
          watch: true,
        },
      },
    },
  });

  grunt.registerTask("watch", ["compass:dev"]);
};
