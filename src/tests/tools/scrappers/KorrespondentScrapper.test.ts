import assert from "assert";
import KorrespondentScrapper from "../../../tools/scrapers/KorrespondentScrapper";

describe("KorrespondentScrapper", function () {
  describe("parseDateTime", function () {
    const timezone = "+0200";

    it("should parse date and time", function () {
      const str = "- 2&nbsp;февраля 2015, 16:12";
      assert.strictEqual(
        new Date(2015, 1, 2, 16, 12).getTime(),
        KorrespondentScrapper.parseDateTime(str, timezone)?.getTime(),
      );
    });

    it("should parse only date", function () {
      assert.strictEqual(
        +new Date(2015, 1, 2, 0, 0),
        KorrespondentScrapper.parseDateTime(
          "- 2&nbsp;февраля 2015",
          timezone,
        )?.getTime(),
      );
    });

    it("should parse only time", function () {
      var now = new Date();
      assert.strictEqual(
        new Date(
          now.getFullYear(),
          now.getMonth(),
          now.getDate(),
          16,
          12,
        ).getTime(),
        KorrespondentScrapper.parseDateTime("16:12", timezone)?.getTime(),
      );
    });

    it("should parse only time with artifact", function () {
      const now = new Date();
      assert.strictEqual(
        new Date(
          now.getFullYear(),
          now.getMonth(),
          now.getDate(),
          22,
          0,
        ).getTime(),
        KorrespondentScrapper.parseDateTime("- 22:00", timezone)?.getTime(),
      );
    });

    it('should parse date time "yesterday"', function () {
      const now = new Date();
      assert.strictEqual(
        new Date(
          now.getFullYear(),
          now.getMonth(),
          now.getDate() - 1,
          15,
          29,
        ).getTime(),
        KorrespondentScrapper.parseDateTime(
          "- Вчера, 15:29",
          timezone,
        )?.getTime(),
      );
    });

    it("should parse only date", function () {
      assert.strictEqual(
        new Date(2020, 6, 22).getTime(),
        KorrespondentScrapper.parseDateTime(
          "- 22 июля 2020",
          timezone,
        )?.getTime(),
      );
    });
  });

  // describe("tokenize", function () {
  //   it("should have exect same tokens", function () {
  //     var str =
  //       "В Калининградской области решением окружного совета депутатов отменено проведение музыкального фестиваля KUBANA.";
  //     var tokens = nlp.tokenize(str);
  //     assert.deepEqual(
  //       [
  //         "в",
  //         "калининградской",
  //         "области",
  //         "решением",
  //         "окружного",
  //         "совета",
  //         "депутатов",
  //         "отменено",
  //         "проведение",
  //         "музыкального",
  //         "фестиваля",
  //         "kubana",
  //       ],
  //       tokens,
  //     );
  //   });
  // });
});
