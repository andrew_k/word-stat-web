const fs = require("fs");

function readByChunks(fd, chunk = 100, position, callback) {
  const buffer = Buffer.alloc(chunk);

  fs.read(fd, buffer, 0, chunk, position, (err, bytesRead, buffer) => {
    console.log(err, bytesRead, buffer.toString("utf-8"));
    if (err) {
      return callback(err);
    }
    callback(null, buffer.toString("utf-8"));

    if (bytesRead === 0) return;
    readByChunks(fd, chunk, position + chunk, callback);
  });
}

