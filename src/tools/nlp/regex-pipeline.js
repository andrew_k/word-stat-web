const fs = require("fs");
const XRegExp = require("xregexp");

const regexpsFile = process.argv[2];
const textFile = process.argv[3];

function getRegexps(file) {
  return JSON.parse(fs.readFileSync(file));
}

function getText(file) {
  return fs.readFileSync(file).toString();
}

function normalizeText(text) {
  return text.toLowerCase();
}

function runPipeline(regexps, initialText) {
  const cleanedText = regexps.reduce((text, reg) => {
    return reg.skip
      ? text
      : text.replace(XRegExp(reg.regexp, "gm"), reg.replacement);
  }, normalizeText(initialText));

  return cleanedText;
}

console.log(runPipeline(getRegexps(regexpsFile), getText(textFile)));
