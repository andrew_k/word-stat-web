var nouns = "глаз сквозь слож крас ночь валюта время смерть".split(" ");
var verbs = "бежа спа смотре".split(" ");

var suffixes = [
  //при добавлении к основе существительного образует прилагательное со значением общей характеристики  лес → лесной
  //при добавлении к основе глагола образует существительное женского рода со значением собирательные понятия  драть → дрань
  "н",
];

var nounsEndings = ["ой"];

var verbsEndings = ["ь"];

verbs.forEach(function(noun) {
  suffixes.forEach(function(suffix) {
    verbsEndings.forEach(function(end) {
      console.log(noun + suffix + end);
    });
  });
});
