import moment from "moment";

export function tokenize(text: String): Array<string> {
  const clean = text
    .toLowerCase()
    .replace(/[^a-zа-яё0-9_-]/gi, " ")
    .replace(/[\s\n]+/g, " ")
    .trim();

  return clean.split(" ");
}

// console.log(parseDateTime("- 22 июля 2020", "+0200"));
