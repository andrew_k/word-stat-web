const RUNNING = "running";
const STOPPED = "stopped";

interface TimerRecord {
  status: string;
  startTime: number;
  stopTime?: number;
}

interface TimersStorage {
  [key: string]: TimerRecord;
}

class Timer {
  timers: TimersStorage = {};

  start(name: string) {
    this.timers[name] = {
      status: RUNNING,
      startTime: +new Date(),
    };
  }
  stop(name: string) {
    if (this.timers[name].status === STOPPED) {
      throw new Error("timer is already stopped");
    }
    this.timers[name].status = STOPPED;
    this.timers[name].stopTime = +new Date();

    return this.get(name);
  }
  get(name: string) {
    if (
      this.timers[name] &&
      this.timers[name].status === STOPPED &&
      this.timers[name].stopTime
    ) {
      return (this.timers[name].stopTime || 0) - this.timers[name].startTime;
    }

    throw new Error("Timer was not started or ended");
  }
}

export default new Timer();
