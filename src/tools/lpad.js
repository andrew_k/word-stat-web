/* eslint-disable no-param-reassign */
function lpad(n, width, z) {
  z = z || "0";
  n = n.toString();

  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}
/* eslint-enable no-param-reassign */

module.exports = lpad;
