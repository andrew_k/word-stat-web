const request = require("request");
const cheerio = require("cheerio");
const config = require("../../config/main.json").scrapers.pravda;
const async = require("async");
const log = require("../log");
const lpad = require("../lpad");
const iconv = require("iconv");

const conv = new iconv.Iconv("windows-1251", "utf8");
const BASE_URL = "http://www.pravda.com.ua";

function fetchMonth(year, month, callback) {
  const date = new Date(year, month - 1, 1);
  const monthDays = [];

  while (date.getMonth() === month - 1) {
    monthDays.push(date.getDate());
    date.setDate(date.getDate() + 1);
  }

  let monthArticles = [];
  async.eachSeries(
    monthDays,
    (day, dayCb) => {
      fetchDay(year, month, day, (err, articles) => {
        if (err) {
          return dayCb(err);
        }
        monthArticles = monthArticles.concat(articles);
        dayCb();
      });
    },
    err => {
      if (err) {
        return callback(err);
      }
      callback(err, monthArticles);
    },
  );
}

function fetchDay(year, month, day, callback) {
  const url = `http://www.pravda.com.ua/rus/archives/date_${lpad(day, 2)}${lpad(
    month,
    2,
  )}${year}/`;

  request(
    {
      url: url,
      encoding: null,
    },
    (err, resp, rawBody) => {
      if (err) {
        return callback(err);
      }

      const body = conv.convert(rawBody).toString();
      const $ = cheerio.load(body, {
        decodeEntities: false,
      });
      const newsHeader = $(
        '.block_news_all .block__head .block__title:contains("Новости")',
      );
      const articles = newsHeader
        .parents(".block_news_all")
        .find(".news_all .article");

      log.info(`${url} articles: ${articles.length}`);
      const articlesData = articles
        .toArray()
        .map(article => getDataFromArticle($, article, year, month, day));

      setTimeout(() => {
        callback(null, articlesData);
      }, config.REQUEST_TIMEOUT);
    },
  );
}

function getDataFromArticle($, art, year, month, day) {
  const createdTimeRaw = $(art)
    .find(".article__time")
    .text()
    .trim();
  const createdTime = createdTimeRaw.split(":");
  const created =
    createdTime.length === 2
      ? new Date(
          year,
          month - 1,
          day,
          parseInt(createdTime[0], 10),
          parseInt(createdTime[1], 10),
        )
      : new Date(year, month - 1, day);

  return {
    title: $(art)
      .find(".article__title a")
      .text()
      .trim(),
    text: $(art)
      .find(".article__subtitle")
      .text()
      .trim(),
    createdRaw: `${year} ${month} ${day} ${createdTimeRaw}`,
    created: created,
    link:
      BASE_URL +
      $(art)
        .find(".article__title a")
        .attr("href"),
  };
}

module.exports = {
  fetchMonth: fetchMonth,
  fetchDay: fetchDay,
  fetchYear: () => {},
};
