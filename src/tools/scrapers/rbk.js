const request = require("request");
const cheerio = require("cheerio");
const config = require("../../config/main.json").scrapers.rbk;
const async = require("async");
const log = require("../log");
const lpad = require("../lpad");

function fetchMonth(year, month, callback) {
  const date = new Date(year, month - 1, 1);
  const monthDays = [];

  while (date.getMonth() === month - 1) {
    monthDays.push(date.getDate());
    date.setDate(date.getDate() + 1);
  }

  let monthArticles = [];
  async.eachSeries(
    monthDays,
    (day, dayCb) => {
      fetchDay(year, month, day, (err, articles) => {
        if (err) {
          return dayCb(err);
        }
        monthArticles = monthArticles.concat(articles);
        dayCb();
      });
    },
    err => {
      if (err) {
        return callback(err);
      }
      callback(err, monthArticles);
    },
  );
}

function fetchDay(year, month, day, callback) {
  const url = `https://www.rbc.ua/rus/archive/${year}/${lpad(month, 2)}/${lpad(
    day,
    2,
  )}`;

  request(url, (err, resp, body) => {
    if (err) {
      return callback(err);
    }
    const $ = cheerio.load(body, { decodeEntities: false });
    const articles = $(".news-feed-content .news-feed-item");
    log.info(`${url} articles: ${articles.length}`);
    const articlesData = articles
      .toArray()
      .map(article => getDataFromArticle($, article, year, month, day));

    setTimeout(() => {
      callback(null, articlesData);
    }, config.REQUEST_TIMEOUT);
  });
}

function getDataFromArticle($, art, year, month, day) {
  const createdTimeRaw = $(art)
    .find(".time")
    .text()
    .trim();
  const createdTime = createdTimeRaw.split(":");
  const created =
    createdTime.length === 2
      ? new Date(
          year,
          month - 1,
          day,
          parseInt(createdTime[0], 10),
          parseInt(createdTime[1], 10),
        )
      : new Date(year, month - 1, day);

  return {
    title: $(art)
      .find("a")
      .clone()
      .find(".time")
      .empty()
      .end()
      .text()
      .trim(),
    text: "",
    createdRaw: `${year} ${month} ${day} ${createdTimeRaw}`,
    created: created,
    link: $(art)
      .find("a")
      .attr("href"),
  };
}

module.exports = {
  fetchMonth: fetchMonth,
  fetchDay: fetchDay,
  fetchYear: () => {},
};
