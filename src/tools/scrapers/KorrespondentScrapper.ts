import axios from "axios";
import jsdom from "jsdom";
import moment from "moment";
const { JSDOM } = jsdom;

import { justText } from "../../utils";
import "../../types/Article";

const monthNames = [
  "january",
  "february",
  "march",
  "april",
  "may",
  "june",
  "july",
  "august",
  "september",
  "october",
  "november",
  "december",
];

interface Config {
  REQUEST_TIMEOUT: number;
  EMPTY_PAGES_ATTEMPTS: number;
}

export default class KorrespondentScrapper {
  public config;

  constructor(config: Config) {
    this.config = config;
  }

  fetchMonthArticlesShort(
    year: number,
    month: number,
    callback: (err?: Error | null, articles?: Array<Article>) => void,
  ) {
    let pageN = 1;
    let emptyPages = 0;
    const monthName = monthNames[month - 1];
    const monthArticles: Array<Article> = [];
    const config = this.config;

    function doRequest() {
      // if (pageN > 1) {
      //   return callback(null, monthArticles);
      // }

      const url = `http://korrespondent.net/all/${year}/${monthName}/p${pageN}`;

      axios
        .get(url)
        .then((response) => {
          const window = new JSDOM(response.data).window;
          const articles = window.document.querySelectorAll(".article");

          console.log(`${url}, articles: ${articles.length}`);

          if (articles.length === 0) {
            emptyPages += 1;
            if (emptyPages >= config.EMPTY_PAGES_ATTEMPTS) {
              callback(null, monthArticles);
            } else {
              pageN += 1;
              doRequest();
            }
          } else {
            emptyPages = 0;
            const parsedArticles: Array<Article> = [];

            articles.forEach((art: Element) => {
              parsedArticles.push(
                KorrespondentScrapper.getDataFromArticle(art),
              );
            });

            monthArticles.push(...parsedArticles);

            setTimeout(() => {
              pageN += 1;
              doRequest();
            }, config.REQUEST_TIMEOUT);
          }
        })
        .catch((error) => {
          console.error(error);
          callback(error);
        });
    }
    doRequest();
  }

  static getDataFromArticle(art: Element): Article {
    const createdEl = art.querySelector(".article__date");
    const createdRaw = justText(createdEl);

    const titleEl = art.querySelector(".article__title a");
    const textEl = art.querySelector(".article__text");
    const linkEl = art.querySelector(".article__title a");

    return {
      title: justText(titleEl),
      text: justText(textEl),
      createdRaw: createdRaw,
      created: createdRaw
        ? KorrespondentScrapper.parseDateTime(createdRaw, "+0200")
        : null,
      url: linkEl ? linkEl.getAttribute("href") : null,
    };
  }

  static parseDateTime(rawDateTime: string, timezone: string): Date | null {
    const dateTimeString = rawDateTime
      .trim()
      .replace("&nbsp;", " ")
      .replace(" ", " ")
      .replace(/^-\s/, "");

    if (/Вчера/.exec(dateTimeString)) {
      const timeString = dateTimeString.replace("Вчера", "");
      const time = moment(timeString, "HH:mmZZ").subtract(1, "day");
      if (time.isValid()) {
        return time.toDate();
      }
    }

    const dateTime = moment(
      `${dateTimeString}${timezone}`,
      ["DD MMMM YYYY, HH:mmZZ", "DD MMMM YYYY", "HH:mmZZ"],
      "ru",
    );

    if (dateTime.isValid()) {
      return dateTime.toDate();
    }

    return null;
  }
}
