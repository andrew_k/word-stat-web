const request = require("request");
const cheerio = require("cheerio");
const config = require("../../config/main.json").scrapers.unn;
const async = require("async");
const log = require("../log");
const lpad = require("../lpad");

const BASE_URL = "http://www.unn.com.ua";

// fetchDay(2017, 01, 11, function (err, items) {
//     console.log(items.length);
// });

function fetchMonth(year, month, callback) {
  const date = new Date(year, month - 1, 1);
  const monthDays = [];

  while (date.getMonth() === month - 1) {
    monthDays.push(date.getDate());
    date.setDate(date.getDate() + 1);
  }

  let monthArticles = [];
  async.eachSeries(
    monthDays,
    (day, dayCb) => {
      fetchDay(year, month, day, (err, articles) => {
        if (err) {
          return dayCb(err);
        }
        monthArticles = monthArticles.concat(articles);
        dayCb();
      });
    },
    err => {
      if (err) {
        return callback(err);
      }
      callback(err, monthArticles);
    },
  );
}

function fetchDay(year, month, day, callback) {
  let currentPage = 1;
  let allArticles = [];

  fetchPage();

  function fetchPage() {
    const url = getUrl(year, month, day, currentPage);
    request(
      {
        url: url,
      },
      (err, resp, body) => {
        if (err) {
          return callback(err);
        }
        const $ = cheerio.load(body, {
          decodeEntities: false,
        });
        const articles = $(".h-news-feed ul li");
        log.info(`${url} articles: ${articles.length}`);
        const articlesData = articles
          .toArray()
          .map(article => getDataFromArticle($, article, year, month, day));

        allArticles = allArticles.concat(articlesData);
        currentPage += 1;

        const pagesNumber = getPagesNumber($);
        log.info("pagesNumber", pagesNumber);
        setTimeout(() => {
          if (currentPage <= pagesNumber) {
            fetchPage();
          } else {
            callback(null, allArticles);
          }
        }, config.REQUEST_TIMEOUT);
      },
    );
  }
}

function getDataFromArticle($, art, year, month, day) {
  const createdTimeRaw = $(art)
    .find(".date")
    .text()
    .trim();
  const createdTime = createdTimeRaw.split(":");
  const created =
    createdTime.length === 2
      ? new Date(
          year,
          month - 1,
          day,
          parseInt(createdTime[0], 10),
          parseInt(createdTime[1], 10),
        )
      : new Date(year, month - 1, day);

  return {
    title: $(art)
      .find(".title a")
      .text()
      .trim(),
    text: "",
    createdRaw: `${year} ${month} ${day} ${createdTimeRaw}`,
    created: created,
    link:
      BASE_URL +
      $(art)
        .find(".title a")
        .attr("href"),
  };
}

function getUrl(year, month, day, pageN) {
  let _url = `http://www.unn.com.ua/ru/news/${year}/${lpad(month, 2)}/${lpad(
    day,
    2,
  )}/`;

  if (pageN > 1) {
    _url += `page-${pageN}/`;
  }

  return _url;
}

function getPagesNumber($) {
  const paginationItems = $(".b-page-selector ul li");
  if (!paginationItems.length) {
    return 1;
  }
  const lastPage = paginationItems[paginationItems.length - 2];

  return parseInt(
    $(lastPage)
      .find("a")
      .text(),
    10,
  );
}

module.exports = {
  fetchMonth: fetchMonth,
  fetchDay: fetchDay,
  fetchYear: () => {},
};
