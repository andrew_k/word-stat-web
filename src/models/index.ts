import mongoose, { Mongoose } from "mongoose";

import config from "../config/db.json";

mongoose.Promise = global.Promise;

let connectedMongoose: Mongoose;
async function connect() {
  const port = config.port ? `:${config.port}` : "";
  const dbUri = `mongodb://${config.host}/${config.database}${port}`;

  if (!connectedMongoose) {
    connectedMongoose = await mongoose.connect(dbUri, {
      user: config.username,
      pass: config.password,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    return connectedMongoose;
  }

  const readyState = connectedMongoose.connection.readyState;
  if (readyState === mongoose.STATES.disconnected) {
    return connectedMongoose.connect(dbUri, {
      user: config.username,
      pass: config.password,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }

  return connectedMongoose;
}

function disconnect() {
  return connectedMongoose.disconnect();
}

// const Message = mongoose.model("message", {
//   userName: String,
//   userNameAliases: Array,
//   displayName: String,
//   messageText: String,
//   created: Date,
//   recipient: String,
//   recipientAliases: Array,
//   sourceName: String,
//   data: mongoose.Schema.Types.Mixed,
// });

// const TelegramMessage = mongoose.model("telegramMessage", {
//   chatName: mongoose.Schema.Types.String,
//   chatId: mongoose.Schema.Types.Number,
//   id: mongoose.Schema.Types.Number,
//   type: mongoose.Schema.Types.String,
//   date: mongoose.Schema.Types.Date,
//   edited: mongoose.Schema.Types.Date,
//   from: mongoose.Schema.Types.String,
//   from_id: mongoose.Schema.Types.Number,
//   text: mongoose.Schema.Types.Mixed,
//   words: mongoose.Schema.Types.Mixed,
//   isText: mongoose.Schema.Types.Boolean,
// });

// const Word = mongoose.model("word", {
//   word: String,
//   created: Date,
//   sourceName: String,
//   sourceId: mongoose.Schema.Types.ObjectId,
//   // sender: String,
//   // recipient: String,
//   // sourceName: String,
// });
//
// const WordArticle = mongoose.model("word_article", {
//   word: String,
//   created: Date,
//   sourceName: String,
//   sourceId: mongoose.Schema.Types.ObjectId,
// });
//
// const AgregatedWord = mongoose.model("agregatedWord", {
//   word: String,
//   count: Number,
//   sourceName: String,
//   sourceId: mongoose.Schema.Types.ObjectId,
// });

const ArticleSchema = new mongoose.Schema({
  url: String,
  title: String,
  text: String,
  createdRaw: String,
  created: Date,
  imported: Date,
  sourceName: String,
});

const Article = mongoose.model("article", ArticleSchema);

// const TextEntity = mongoose.model("textEntity", {
//   title: String,
//   text: String,
//   created: Date,
//   processed: Boolean,
// });
//
// const Bigram = mongoose.model("bigram", {
//   word1: String,
//   word2: String,
//   stem1: String,
//   stem2: String,
//   sourceName: String,
// });

export default {
  connect,
  disconnect,
  // Message,
  // Word,
  // WordArticle,
  Article,
  // TextEntity,
  // AgregatedWord,
  // Bigram,
  // TelegramMessage,
};
