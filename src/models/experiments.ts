import models from "./index";

models.connect().then(async () => {
  const result = await models.Article.aggregate([
    {
      $match: { created: { $gt: new Date(2020, 0, 1) } },
    },
    {
      $group: {
        _id: {
          // year: { $year: "$created" },
          month: { $month: "$created" },
          // day: { $dayOfWeek: "$created" },
        },
        count: { $sum: 1 },
      },
    },
    // {
    //   $project: {
    //     date: {
    //       $concat: [
    //         { $toString: "$_id.year" },
    //         "-",
    //         { $toString: "$_id.month" },
    //       ],
    //     },
    //     _id: 0,
    //     count: 1,
    //   },
    // },
    { $sort: { "_id.month": 1 } },
  ]);
  console.log(result);

  await models.disconnect();
});
