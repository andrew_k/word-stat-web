import models from "./index";
import KorrespondentScrapper from "../tools/scrapers/KorrespondentScrapper";

models.connect().then(async () => {
  const result = await models.Article.find({
    createdRaw: /^- \d{1,2}? .+ \d{4}$/,
  });

  for (let doc of result) {
    const res = await models.Article.updateOne(
      { _id: doc.id },
      { created: KorrespondentScrapper.parseDateTime(doc.createdRaw, "+0200") },
    ).exec();
    console.log(res);
  }

  // console.log(result[0]);

  await models.disconnect();
});
