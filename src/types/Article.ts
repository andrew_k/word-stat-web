interface Article {
  url: string | null;
  title: string | null;
  text: string | null;
  createdRaw: string | null;
  created: Date | null;
  imported?: Date | null;
}
