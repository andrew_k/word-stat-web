import { ErrorCallback } from "async";
import _ from "lodash";
import async from "async";

import KorrespondentScrapper from "./tools/scrapers/KorrespondentScrapper";

import config from "./config/main.json";
// @ts-ignore
import models from "./models";
// const log = (...rest) => console.log.apply(console, rest);
import timer from "./tools/timer";

const source = config.run_source;
// @ts-ignore
const fetcherConfig = config.fetchers[source];
const scraper = new KorrespondentScrapper(config.scrapers.korr);
const ArticleModel = models.Article;

(async function () {
  await models.connect();

  timer.start("fetching");
  console.log(`start fetching ${source}`);

  const years = fetcherConfig.years.range
    ? _.range(fetcherConfig.years.range[0], fetcherConfig.years.range[1] + 1)
    : fetcherConfig.years;

  const months = fetcherConfig.months.range
    ? _.range(fetcherConfig.months.range[0], fetcherConfig.months.range[1] + 1)
    : fetcherConfig.months;

  console.log("years", years);
  console.log("months", months);

  const importDate = new Date();

  async.eachSeries(
    years,
    (year: number, yearDoneCallback: ErrorCallback) => {
      async.eachSeries(
        months,
        (month: number, monthDoneCallback: ErrorCallback) => {
          scraper.fetchMonthArticlesShort(
            year,
            month,
            (err, articles?: Array<Article>) => {
              if (err) {
                monthDoneCallback(err);
              } else if (articles) {
                saveArticlesBatch(articles, monthDoneCallback);
              } else {
                monthDoneCallback();
              }
            },
          );
        },
        yearDoneCallback,
      );
    },
    allYearsDoneCallback,
  );

  function saveArticlesBatch(
    articles: Array<Article>,
    callback: ErrorCallback,
  ) {
    console.log("saveArticlesBatch", articles.length);

    const urls = articles.map((a) => a.url);
    // console.log("urls", urls);
    models.Article.find({ url: { $in: urls } }).then((existedArticles) => {
      console.log("exists articles", existedArticles.length);

      const insertArticles = articles
        .filter(
          (article) => !existedArticles.find((ea) => ea.url === article.url),
        )
        .map((article) => {
          return {
            ...article,
            sourceName: fetcherConfig.sourceName,
            imported: importDate,
          };
        });

      console.log("insert articles", insertArticles.length);

      models.Article.insertMany(
        insertArticles,
        {
          ordered: false,
        },
        (error, docs) => {
          if (error) {
            console.error(error);
          }
          if (docs) {
            console.log("saved", docs.length);
            const totalSaved = increaseCounter(docs.length);
            console.log("total articles saved", totalSaved);
          }
          callback(error);
        },
      );
    });

    // async.eachSeries(
    //   articles,
    //   (article: Article, articleSavedCb: ErrorCallback) => {
    //     const articleToSave = _.extend({}, article, {
    //       sourceName: fetcherConfig.sourceName,
    //       imported: importDate,
    //     });
    //     ArticleModel.findOne({ url: articleToSave.url })
    //       .exec()
    //       .then((data) => {
    //         if (data) {
    //           return;
    //         }
    //         return new ArticleModel(articleToSave).save().then(increaseCounter);
    //       })
    //       .then(() => articleSavedCb())
    //       .catch((error: any) => {
    //         console.log(error);
    //         articleSavedCb(error);
    //       });
    //   },
    //   callback,
    // );
  }

  let articlesSaved = 0;
  function increaseCounter(n = 1) {
    articlesSaved += n;

    return articlesSaved;
  }

  function allYearsDoneCallback(err?: Error | null) {
    if (err) {
      return console.error("Error: ", err);
    }
    console.log(
      `years: ${years.join(",") || "NONE"}, months: ${
        months.join(",") || "NONE"
      } are processed`,
    );
    console.log(
      `${new Date().toLocaleTimeString()} saved ${articlesSaved} articles`,
    );
    console.log(`import took ${timer.stop("fetching") / 1000} seconds`);
    models.disconnect();
  }
})();
