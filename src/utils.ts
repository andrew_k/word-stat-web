export function justText(element: Element | null): null | string {
  if (element) {
    return Array.prototype.filter
      .call(
        element.childNodes,
        (child) =>
          child.nodeType === child.ownerDocument.defaultView.Node.TEXT_NODE,
      )
      .map((child) => child.textContent)
      .join(" ")
      .trim();
  }
  return null;
  // return el.clone().children().remove().end().text();
}
