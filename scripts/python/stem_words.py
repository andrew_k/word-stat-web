# -*- coding: utf-8 -*-
import pymongo
import argparse
from nltk.stem.snowball import RussianStemmer
from nltk.tokenize.api import StringTokenizer

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-s', required=True, help='sourceName')

args = parser.parse_args()

client = pymongo.MongoClient()
db = client['word-stat']
cWords = db['words']

words = cWords.find({"sourceName": args.s})

r = RussianStemmer()
for w in words:
    stem = r.stem(w.get('word', ''))
    cWords.update({'_id': w['_id']}, {'$set': {'stem': stem}})

