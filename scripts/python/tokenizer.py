# coding=utf-8

import re
import pymongo


client = pymongo.MongoClient()
db = client['word-stat']
cTextEntities = db['textentities']

ent = cTextEntities.find_one({"title": u'Анна Каренина'})

def tokenize(text):
    regex_no_punkt = re.compile(u'[^a-zа-яё0-9]', re.IGNORECASE | re.UNICODE)
    regex_ok_spaces = re.compile(u'[\s\n]+')
    cleaned = regex_no_punkt.sub(' ', text)
    return regex_ok_spaces.sub(' ', cleaned).split(' ')

print len(tokenize(ent['text']))
