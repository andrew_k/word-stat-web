# -*- coding: utf-8 -*-

from nltk.stem.snowball import RussianStemmer
from nltk.tokenize.api import StringTokenizer
import pymongo
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('-s', required=True, help='sourceName')

args = parser.parse_args()


client = pymongo.MongoClient()
db = client['word-stat']
cBigrams = db['bigrams'];

r = RussianStemmer()

bigrams = cBigrams.find({
	"sourceName": args.s
});

for b in bigrams:
	stem1 = r.stem(b['word1'])
	stem2 = r.stem(b.get('word2', ''))
	cBigrams.update({'_id': b['_id']}, {'$set': {'stem1': stem1, 'stem2': stem2}})

