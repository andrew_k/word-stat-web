const fetch = require("node-fetch");
const cheerio = require("cheerio");

const startPage = "http://masteroff.org/band/Oxxxymiron";
const targetLinkSelector = "#content tr .cell-first a";
const targetContentSelector = "#content .songtext";

process.on("unhandledRejection", function(reason, p) {
  console.log("Unhandled Rejection:", reason.stack);
});

function getTargetLinks(pageUrl, linkSelector) {
  return fetch(pageUrl)
    .then(res => {
      return res.text();
    })
    .then(text => {
      // console.log(text);
      const $ = cheerio.load(text);

      const links = [];
      $(linkSelector).each((i, link) => {
        links.push({
          idx: i,
          title: $(link).text(),
          url: $(link).attr("href"),
        });
      });

      return links;
    });
}

function getTargetContent(link, contentSelector) {
  return fetch(link.url)
    .then(res => {
      return res.text();
    })
    .then(text => {
      const $ = cheerio.load(text);

      return Object.assign(
        {
          content: $(contentSelector).text(),
        },
        link,
      );
    });
}

function getAllTargetsContent(pageUrl, linkSelector, contentSelector) {
  return getTargetLinks(pageUrl, linkSelector).then(links => {
    console.log(links);
    return;
    var promises = links.slice(92, 101).map((link, i) => {
      return delayP(() => getTargetContent(link, contentSelector), 100 * i);
    });

    return Promise.all(promises);
  });
}

getAllTargetsContent(startPage, targetLinkSelector, targetContentSelector).then(
  targetsContent => {
    targetsContent.forEach(content => {
      console.log();
      console.log("======" + content.idx + "======");
      console.log(content.url);
      console.log(content.title);
      console.log();
      console.log(content.content);
    });
  },
);

function delayP(fn, delay) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(fn());
    }, delay);
  });
}
