const minimist = require("minimist");

function output(title, data) {
  console.log(title);
  console.dir(data, { depth: null });
}

function outputCSV(data) {
  console.log(
    data.reduce((output, row) => {
      output += row.result.join(",") + "\n";
      return output;
    }, ""),
  );
}

function jsonOutput(title, data) {
  console.log(title);
  console.log(JSON.stringify(data, "", 2));
}

function getArgs() {
  return minimist(process.argv.slice(2));
}

function withConsoleArgs(fn) {
  return fn(getArgs());
}

module.exports = {
  output,
  jsonOutput,
  getArgs,
  withConsoleArgs,
  outputCSV,
};
