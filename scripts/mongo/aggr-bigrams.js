var res = db.bigrams.aggregate(
  {
    $match: {
      // sourceName: 'Виктор Пелевин. Принц Госплана'
      sourceName: "Шатуны",
    },
  },
  {
    $group: {
      _id: {
        stem1: "$stem1",
        stem2: "$stem2",
      },
      count: {
        $sum: 1,
      },
      words1: {
        $addToSet: "$word1",
      },
      words2: {
        $addToSet: "$word2",
      },
    },
  },
  {
    $sort: {
      count: -1,
    },
  },
);

printjson(res.toArray());
