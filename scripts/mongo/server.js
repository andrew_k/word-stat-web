const express = require("express");
const cors = require("cors");

const totalWords = require("./telegram/totalWords");
const models = require("../../src/models/index");

const app = express();

app.use(cors());

app.get("/totalWords", async (req, res) => {
  res.json(await totalWords(req.query));
});

(async function start() {
  await models.connect();

  app.listen(4321, () => {
    console.log("Server started");
  });
})();
