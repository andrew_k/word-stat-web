db.getCollection("words").aggregate([
  { $match: { sourceName: "tagor" } },
  { $group: { _id: "$word", count: { $sum: 1 } } },
  { $sort: { count: -1 } },
]);
