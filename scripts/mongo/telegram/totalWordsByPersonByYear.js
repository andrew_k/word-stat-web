const models = require("../../../src/models/index");
const { jsonOutput, getArgs } = require("../utils");

async function getData({ word, from, messages, year }) {
  await models.connect();

  const totalWords = await models.TelegramMessage.aggregate(
    [
      { $match: { from: { $ne: null } } },
      from ? { $match: { from: new RegExp(from) } } : undefined,
      { $unwind: "$words" },
      { $match: { words: { $ne: "" } } },
      { $match: { words: { $ne: "-" } } },
      word ? { $match: { words: new RegExp(word) } } : undefined,
      {
        $group: {
          _id: {
            from: "$from",
            // word: "$words",
            date: {
              $concat: [
                { $toString: { $year: "$date" } },
                "_",
                {
                  $cond: [
                    { $lte: [{ $month: "$date" }, 9] },
                    {
                      $concat: ["0", { $substr: [{ $month: "$date" }, 0, 2] }],
                    },
                    { $substr: [{ $month: "$date" }, 0, 2] },
                  ],
                },
              ],
            },
          },
          ...(messages ? { messages: { $addToSet: "$text" } } : {}),
          count: { $sum: 1 },
        },
      },
      // { $sort: { count: -1 } },
      // {
      //   $group: {
      //     _id: {
      //       word: "$_id.word",
      //     },
      //     people: {
      //       $addToSet: {
      //         from: "$_id.from",
      //         count: "$count",
      //       },
      //     },
      //     count: { $sum: "$count" },
      //   },
      // },
      { $sort: { "_id.date": 1 } },
      {
        $group: {
          _id: { from: "$_id.from" },
          data: { $push: { date: "$_id.date", count: "$count" } },
        },
      },
    ].filter(p => p),
  );

  jsonOutput("", totalWords);

  await models.disconnect();
}

getData(getArgs());
