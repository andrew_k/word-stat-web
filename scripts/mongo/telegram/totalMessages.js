const models = require("../../../src/models/index");
const { output } = require("../utils");

(async function() {
  await models.connect();

  // const totalCount = await models.TelegramMessage.aggregate([
  //   { $match: { from: { $ne: null } } },
  //   { $group: { _id: null, count: { $sum: 1 } } },
  // ]);
  // console.log("totalCount", totalCount);
  //
  // const countByPerson = await models.TelegramMessage.aggregate([
  //   { $match: { from: { $ne: null } } },
  //   { $group: { _id: "$from", count: { $sum: 1 } } },
  //   { $sort: { count: -1 } },
  // ]);
  // console.log("countByPerson", countByPerson);

  // const countByPersonByYear = await models.TelegramMessage.aggregate([
  //   { $match: { from: { $ne: null } } },
  //   {
  //     $group: {
  //       _id: { from: "$from", year: { $year: "$date" } },
  //       count: { $sum: 1 },
  //     },
  //   },
  //   { $sort: { "_id.year": 1 } },
  //   {
  //     $group: {
  //       _id: { from: "$_id.from" },
  //       messages: { $push: { year: "$_id.year", count: "$count" } },
  //     },
  //   },
  // ]);
  //
  // console.log("countByPersonByYear");
  // console.dir(countByPersonByYear, {depth: null});

  // const countByPersonByMonth = await models.TelegramMessage.aggregate([
  //   { $match: { from: { $ne: null } } },
  //   {
  //     $group: {
  //       _id: {
  //         from: "$from",
  //         date: {
  //           $concat: [
  //             { $toString: { $year: "$date" } },
  //             "_",
  //             { $toString: { $month: "$date" } },
  //           ],
  //         },
  //       },
  //       count: { $sum: 1 },
  //     },
  //   },
  //   { $sort: { "_id.date": 1 } },
  //   {
  //     $group: {
  //       _id: { from: "$_id.from" },
  //       messages: { $push: { date: "$_id.date", count: "$count" } },
  //     },
  //   },
  // ]);
  //
  // console.log("countByPersonByMonth");
  // console.dir(countByPersonByMonth, { depth: null });

  // const countByWeekDay = await models.TelegramMessage.aggregate([
  //   { $match: { from: { $ne: null } } },
  //   {
  //     $group: {
  //       _id: {
  //         // from: "$from",
  //         date: { $dayOfWeek: "$date" },
  //       },
  //       count: { $sum: 1 },
  //     },
  //   },
  //   { $sort: { "_id.date": 1 } },
  // ]);
  //
  // console.log("countByWeekDay");
  // console.dir(countByWeekDay, { depth: null });

  // const countByWeekDayByPerson = await models.TelegramMessage.aggregate([
  //   { $match: { from: { $ne: null } } },
  //   {
  //     $group: {
  //       _id: {
  //         from: "$from",
  //         date: { $dayOfWeek: "$date" },
  //       },
  //       count: { $sum: 1 },
  //     },
  //   },
  //   { $sort: { "_id.date": 1 } },
  //   {
  //     $group: {
  //       _id: { from: "$_id.from" },
  //       messages: { $push: { date: "$_id.date", count: "$count" } },
  //     },
  //   },
  // ]);
  //
  // console.log("countByWeekDayByPerson");
  // console.dir(countByWeekDayByPerson, { depth: null });

  // console.log("countByDayOfMonth");
  // console.dir(
  //   await models.TelegramMessage.aggregate([
  //     { $match: { from: { $ne: null } } },
  //     {
  //       $group: {
  //         _id: {
  //           // from: "$from",
  //           date: { $dayOfMonth: "$date" },
  //         },
  //         count: { $sum: 1 },
  //       },
  //     },
  //     { $sort: { "_id.date": 1 } },
  //     // {
  //     //   $group: {
  //     //     _id: { from: "$_id.from" },
  //     //     messages: { $push: { date: "$_id.date", count: "$count" } },
  //     //   },
  //     // },
  //   ]),
  //   { depth: null },
  // );

  // console.log("countByDayOfMonthByPerson");
  // console.dir(
  //   await models.TelegramMessage.aggregate([
  //     { $match: { from: { $ne: null } } },
  //     {
  //       $group: {
  //         _id: {
  //           from: "$from",
  //           date: { $dayOfMonth: "$date" },
  //         },
  //         count: { $sum: 1 },
  //       },
  //     },
  //     { $sort: { "_id.date": 1 } },
  //     {
  //       $group: {
  //         _id: { from: "$_id.from" },
  //         messages: { $push: { date: "$_id.date", count: "$count" } },
  //       },
  //     },
  //   ]),
  //   { depth: null },
  // );

  // console.log("countByHour");
  // console.dir(
  //   await models.TelegramMessage.aggregate([
  //     { $match: { from: { $ne: null } } },
  //     {
  //       $group: {
  //         _id: {
  //           // from: "$from",
  //           date: { $hour: "$date" },
  //         },
  //         count: { $sum: 1 },
  //       },
  //     },
  //     { $sort: { "_id.date": 1 } },
  //     // {
  //     //   $group: {
  //     //     _id: { from: "$_id.from" },
  //     //     messages: { $push: { date: "$_id.date", count: "$count" } },
  //     //   },
  //     // },
  //   ]),
  //   { depth: null },
  // );

  output(
    "countByHourByPerson",
    await models.TelegramMessage.aggregate([
      { $match: { from: { $ne: null } } },
      {
        $group: {
          _id: {
            from: "$from",
            date: { $hour: "$date" },
          },
          count: { $sum: 1 },
        },
      },
      { $sort: { "_id.date": 1 } },
      {
        $group: {
          _id: { from: "$_id.from" },
          messages: { $push: { date: "$_id.date", count: "$count" } },
        },
      },
    ]),
    { depth: null },
  );

  await models.disconnect();
})();
