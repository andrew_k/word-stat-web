const models = require("../../../src/models/index");
const { withConsoleArgs, outputCSV } = require("../utils");

module.exports = ({ chat }) =>
  models.TelegramMessage.aggregate(
    [
      chat ? { $match: { chatName: chat } } : undefined,
      { $match: { from: { $ne: null } } },
      { $unwind: "$words" },
      { $match: { words: { $ne: "" } } },
      { $match: { words: { $ne: "-" } } },
      // { $match: { words: /пизд/ } },
      {
        $group: {
          _id: {
            from: "$from",
            // word: "$words",
            // date: { $hour: "$date" },
          },
          count: { $sum: 1 },
        },
      },
      // { $sort: { "_id.date": 1 } },
      { $sort: { count: -1 } },
      { $project: { _id: 0, from: "$_id.from", count: 1 } },
      // { $project: { result: ["$_id.from", "$count"] } },
      // {
      //   $group: {
      //     _id: { from: "$_id.from" },
      //     messages: { $push: { date: "$_id.date", count: "$count" } },
      //   },
      // },
    ].filter((f) => !!f),
  );
