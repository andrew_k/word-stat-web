const models = require("../../../src/models/index");
const { jsonOutput, getArgs } = require("../utils");

async function getData({ chat, word, from, date, fullMessages }) {
  await models.connect();

  const totalWords = await models.TelegramMessage.aggregate(
    [
      { $match: { from: { $ne: null } } },
      from ? { $match: { from: new RegExp(from) } } : undefined,
      chat ? { $match: { chatName: chat.toString() } } : undefined,
      { $unwind: "$words" },
      { $match: { words: { $ne: "" } } },
      { $match: { words: { $ne: "-" } } },
      word ? { $match: { words: new RegExp(word) } } : undefined,
      {
        $group: {
          _id: {
            from: "$from",
            // word: "$words",
            ...(date === "month"
              ? {
                  date: {
                    $concat: [
                      { $toString: { $year: "$date" } },
                      "_",
                      {
                        $cond: [
                          { $lte: [{ $month: "$date" }, 9] },
                          {
                            $concat: [
                              "0",
                              { $substr: [{ $month: "$date" }, 0, 2] },
                            ],
                          },
                          { $substr: [{ $month: "$date" }, 0, 2] },
                        ],
                      },
                    ],
                  },
                }
              : {}),
            ...(date === "day"
              ? {
                  date: { $dayOfWeek: "$date" },
                }
              : {}),
          },
          messages: { $addToSet: "$id" },
          ...(fullMessages ? { fullMessages: { $addToSet: "$text" } } : {}),
          count: { $sum: 1 },
        },
      },
      {
        $project: {
          messages: { $size: "$messages" },
          words: "$count",
          fullMessages: 1,
          ...(fullMessages ? { fullMessages: 1 } : {}),
        },
      },
      {
        $project: {
          messages: 1,
          words: 1,
          ratio: { $divide: ["$words", "$messages"] },
          ...(fullMessages ? { fullMessages: 1 } : {}),
        },
      },
      {
        $project: {
          data: {
            messages: "$messages",
            words: "$words",
            ratio: "$ratio",
            ...(fullMessages ? { fullMessages: "$fullMessages" } : {}),
          },
        },
      },
      // { $sort: { count: -1 } },
      // {
      //   $group: {
      //     _id: {
      //       word: "$_id.word",
      //     },
      //     people: {
      //       $addToSet: {
      //         from: "$_id.from",
      //         count: "$count",
      //       },
      //     },
      //     count: { $sum: "$count" },
      //   },
      // },
      { $sort: { "data.messages": -1 } },
      date && { $sort: { "_id.date": 1 } },
      date && {
        $group: {
          _id: { from: "$_id.from" },
          data: { $push: { date: "$_id.date", data: "$data" } },
        },
      },
    ].filter((p) => p),
  );

  jsonOutput("", totalWords);

  await models.disconnect();
}

getData(getArgs());

db.getCollection("articles").aggregate([
  {
    $group: {
      _id: {
        year: { $year: "$created" },
        month: { $month: "$created" },
      },
      count: { $sum: 1 },
    },
  },
  {
    $project: {
      date: {
        $concat: [{ $toString: "$_id.year" }, "-", { $toString: "$_id.month" }],
      },
      _id: 0,
    },
  },
]);
