const models = require("../../../src/models/index");
const { jsonOutput, getArgs } = require("../utils");

async function getData({ word, from }) {
  await models.connect();

  const totalWords = await models.TelegramMessage.aggregate(
    [
      { $match: { from: { $ne: null } } },
      from ? { $match: { from: new RegExp(from) } } : undefined,
      { $unwind: "$words" },
      { $match: { words: { $ne: "" } } },
      { $match: { words: { $ne: "-" } } },
      word ? { $match: { words: new RegExp(word) } } : undefined,
      {
        $group: {
          _id: {
            // from: "$from",
            word: "$words",
          },
          messages: { $addToSet: "$text" },
          count: { $sum: 1 },
        },
      },
      // { $sort: { "_id.date": 1 } },
      { $sort: { count: -1 } },
      // {
      //   $group: {
      //     _id: { from: "$_id.from" },
      //     messages: { $push: { date: "$_id.date", count: "$count" } },
      //   },
      // },
    ].filter(p => p),
  );

  jsonOutput("", totalWords);

  await models.disconnect();
}

getData(getArgs());
