const _ = require("lodash");

const a = [
  {
    _id: {
      from: "Вова Стрелец",
    },
    count: 799,
  },
  {
    _id: {
      from: "Шнапс",
    },
    count: 782,
  },
  {
    _id: {
      from: "Andrew K",
    },
    count: 217,
  },
  {
    _id: {
      from: "Андрюша Стрелец",
    },
    count: 63,
  },
];

const b = [
  {
    _id: {
      from: "Шнапс",
    },
    count: 30003,
  },
  {
    _id: {
      from: "Вова Стрелец",
    },
    count: 28148,
  },
  {
    _id: {
      from: "Andrew K",
    },
    count: 9145,
  },
  {
    _id: {
      from: "Андрюша Стрелец",
    },
    count: 2453,
  },
];

function convert(array) {
  return array.reduce((res, it) => {
    res[it._id.from] = it.count;
    return res;
  }, {});
}

function div(aOrig, bOrig) {
  const a = { ...aOrig };
  const b = { ...bOrig };
  const res = {};

  Object.keys(a).forEach(name => {
    res[name] = Math.round((a[name] / b[name]) * 10000) / 10000;
  });

  return res;
}

console.log(_.toPairs(div(convert(a), convert(b))).sort((a, b) => a[1] - b[1]));
