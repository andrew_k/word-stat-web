const models = require("../../../src/models/index");
const { jsonOutput, getArgs } = require("../utils");

async function getData({ from, messages, date }) {
  await models.connect();

  const totalWords = await models.TelegramMessage.aggregate(
    [
      // { $match: { from: { $ne: null } } },
      from ? { $match: { from: new RegExp(from) } } : undefined,
      {
        $group: {
          _id: {
            chatName: "$chatName",
            // word: "$words",
            ...(date === "month"
              ? {
                  date: {
                    $concat: [
                      { $toString: { $year: "$date" } },
                      "_",
                      {
                        $cond: [
                          { $lte: [{ $month: "$date" }, 9] },
                          {
                            $concat: [
                              "0",
                              { $substr: [{ $month: "$date" }, 0, 2] },
                            ],
                          },
                          { $substr: [{ $month: "$date" }, 0, 2] },
                        ],
                      },
                    ],
                  },
                }
              : {}),
            ...(date === "day"
              ? {
                  date: { $dayOfWeek: "$date" },
                }
              : {}),
          },
          // ...(messages ? { messages: { $addToSet: "$id" } } : {}),
          count: { $sum: 1 },
        },
      },
      { $sort: { count: -1 } },
      date && { $sort: { "_id.date": 1 } },
      date && {
        $group: {
          _id: { from: "$_id.from" },
          data: { $push: { date: "$_id.date", data: "$data" } },
        },
      },
    ].filter(p => p),
  );

  jsonOutput("", totalWords);

  await models.disconnect();
}

getData(getArgs());
