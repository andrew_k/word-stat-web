const spawn = require("child_process").spawn;
const log = require("../../src/tools/log");
const config = require("../../src/config/db");
const path = require("path");

const appRoot = `${__dirname}/../../`;

const date = new Date().toString().replace(/([^\w])/g, "_");
const dumpFolder = path.normalize(appRoot + config.dump.folder);

const mongocmd = "mongodump";
const params = [
  "-d",
  config.database,
  "--gzip",
  `--archive=${dumpFolder}${config.database}__${date}.zip.archive`,
];

log.info("============= mongo dump start =============");
log.info(`\n$ ${mongocmd} ${params.join(" ")} \n`);

const cmdProcess = spawn(mongocmd, params);
cmdProcess.stdout.on("data", data => {
  log.info(data.toString());
});

cmdProcess.stderr.on("data", data => {
  log.info(data.toString());
});

cmdProcess.on("exit", () => {
  log.info("============= mongo dump finished =============");
});
