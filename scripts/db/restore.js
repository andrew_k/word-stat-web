const spawn = require("child_process").spawn;
const fs = require("fs");
const log = require("../../src/tools/log");
const config = require("../../src/config/db");

const appRoot = `${__dirname}/../../`;

const dumpFolder = appRoot + config.dump.folder;
const files = fs
  .readdirSync(dumpFolder)
  .map(fileName => ({
    name: fileName,
    time: fs.statSync(dumpFolder + fileName).mtime.getTime(),
  }))
  .sort((a, b) => b.time - a.time);

if (!files.length) {
  log.info("no available dumps");
  process.exit();
}

const mongocmd = "mongorestore";
const lastDump = `${dumpFolder}/${files[0].name}`;
const params = ["-d", config.database, "--gzip", `--archive=${lastDump}`];

log.info("============= mongo restore start =============");
log.info(`\n$ ${mongocmd} ${params.join(" ")} \n`);

const cmdProcess = spawn(mongocmd, params);
cmdProcess.stdout.on("data", data => {
  log.info(data.toString());
});

cmdProcess.stderr.on("data", data => {
  log.info(data.toString());
});

cmdProcess.on("exit", () => {
  log.info("============= mongo restore finished =============");
});
