var request = require("request");
var cheerio = require("cheerio");
var async = require("async");
var _ = require("underscore");
var models = require("../../src/models");
var mongoose = require("mongoose");
var nlp = require("../../nlp");

mongoose.connect("mongodb://localhost/word-stat");

models.Article.find({
  created: null,
}).exec(function(err, items) {
  console.log(items.length);

  _.each(items, function(it) {
    var dateTime = nlp.parseDateTime(it.createdRaw);
    console.log(it.createdRaw);
    console.log(dateTime);
    models.Article.update(
      {
        _id: it._id,
      },
      {
        $set: {
          created: dateTime,
        },
      },
      {},
      function(err) {
        if (err) {
          console.log("ERROR:", err);
        }
      },
    );
  });

  // console.log(items);
});
