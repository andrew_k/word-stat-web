var mongoose = require("mongoose");
var mod = require("../../src/models");
var async = require("async");
var _ = require("underscore");
var nlp = require("../../nlp");
var minimist = require("minimist");

mongoose.connect("mongodb://localhost/word-stat");

//READ COMMAND LINE ARGS
var args = minimist(process.argv.slice(2));
if (!_.isString(args["s"]) || args["s"] == "") {
  console.log("Wrong args:");
  console.log("");
  console.log("specify -s [sourceName]");
  return;
}
var SOURCE_NAME = args["s"];

mod.TextEntity.find({
  title: SOURCE_NAME,
}).exec(function(err, items) {
  var counter = 0;
  async.eachSeries(
    items,
    function(it, itCb) {
      console.log("Tokenizing", it.title, "....");

      var tokens = nlp.tokenize(it.text);
      console.log("Tokens: ", tokens.length);
      var idx = 0;
      async.whilst(
        function() {
          return idx < tokens.length;
        },
        function(whCb) {
          idx++;
          new mod.Word({
            word: tokens[idx],
            sourceName: it.title,
          }).save(whCb);
          console.log("Saved", idx, "/", tokens.length);
        },
        function(err) {
          if (err) {
            console.log("Error:", err);
          } else {
            console.log("Words saved", it.title, idx);
          }
          itCb(err);
        },
      );
    },
    function(err) {
      if (err) {
        console.log("Error", err);
      }
      console.log("Finished!");
      mongoose.connection.close();
    },
  );
});

// console.log(text);
// console.log(tokenize(text));
