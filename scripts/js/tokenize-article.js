const mongoose = require("mongoose");
const async = require("async");
const _ = require("underscore");
const nlp = require("../../nlp");

mongoose.connect("mongodb://localhost/word-stat");
const Article = require("../../src/models").Article;
const WordArticle = require("../../src/models").WordArticle;

const SOURCE_NAME = "korr";

async.series(
  [
    cb => {
      WordArticle.remove({
        sourceName: SOURCE_NAME,
      }).exec(() => {
        console.log("Articles removed");
        cb();
      });
      // cb();
    },
    cb => {
      Article.find({
        created: {
          $gt: new Date(2014, 0, 1),
          // $lt: new Date(2015, 0, 0)
        },
      }).exec((err, items) => {
        // console.log('Articles: ', items.length, err);
        // return;

        let counter = 0;
        async.eachSeries(
          items,
          (it, itCb) => {
            const titleTokens = nlp.tokenize(it.title);
            const textTokens = nlp.tokenize(it.text);

            const uniqTokens = _.uniq(titleTokens.concat(textTokens));
            // console.log(uniqTokens);
            async.each(
              uniqTokens,
              (tok, tokCb) => {
                const word = new WordArticle({
                  word: tok,
                  created: it.created,
                  sourceName: SOURCE_NAME,
                  sourceId: it._id,
                });
                word.save(_err => {
                  tokCb(_err);
                });
              },
              _err => {
                counter += 1;
                console.log("Saved", counter, "articles", "from", items.length);
                itCb(_err);
              },
            );
          },
          _err => {
            if (_err) {
              console.log("Error", _err);
            }
            console.log("Finished!");
            mongoose.connection.close();
            cb(_err);
          },
        );
      });
    },
  ],
  err => {
    if (err) {
      console.log("Error", err);
    }
  },
);
