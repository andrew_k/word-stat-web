var lineReader = require("line-reader");
var fs = require("fs");
var util = require("util");
var _ = require("underscore");
var mongoose = require("mongoose");
var models = require("../models");
var Message = models.Message;
var async = require("async");
var Iconv = require("iconv").Iconv;

mongoose.connect("mongodb://localhost/word-stat");

var qipHistoryFolder = "/home/andrew/Documents/QIP/Users/351476665/History/";
var myName = "andrew_k49";

var files = fs.readdirSync(qipHistoryFolder);

var iconv = new Iconv("CP1251", "UTF-16LE");
async.eachSeries(
  files,
  function(fileName, callback) {
    if (fileName == "_srvlog.txt") return callback();

    var content = fs.readFileSync(qipHistoryFolder + "/" + fileName);
    var text = iconv.convert(content).toString("utf16le");
    var parsed = parseQipMessages(text);
    // console.log(parsed);
    // return;

    parsed.messages.forEach(function(m) {
      if (m.direction == ">") {
        m.userName = myName;
        m.userNameAliases = parsed.names[">"];
        m.recipient = parsed.names["<"][0];
        m.recipientAliases = parsed.names["<"];
      } else {
        m.userName = parsed.names["<"][0];
        m.recipient = myName;
        m.userNameAliases = parsed.names["<"];
      }
      m.sourceName = "qip";
      // delete m.direction;
    });

    async.each(
      parsed.messages,
      function(m, cb) {
        var message = new Message(m);
        message.save(cb);
      },
      function(err) {
        if (err) {
          console.log("Error" + err);
          callback(err);
        }
        console.log("finished", parsed.names);
        callback();
      },
    );
  },
  function(err) {
    if (err) console.log("Error" + err);
    mongoose.connection.close();
  },
);

function parseQipMessages(text) {
  var messageDirection; //send, recieve
  var inMessage = false;
  var messageLine = 0;
  var messageData = null;
  var messages = [];
  var names = {
    ">": [],
    "<": [],
  };
  var previousName;

  text.split("\n").forEach(function(line) {
    var regRes;
    line = line.trim();
    if ((regRes = /^\-+(\<|\>)-$/.exec(line))) {
      if (messageData !== null) messages.push(messageData);

      messageDirection = regRes[1];
      messageData = {
        direction: messageDirection,
        created: null,
        messageText: "",
      };
      messageLine = 1;
    } else if (messageLine == 1) {
      regRes = /(.+) \((.+)\)/.exec(line);
      if (names[messageDirection].indexOf(regRes[1]) === -1) {
        names[messageDirection].push(regRes[1]);
      }

      var dateAndTime = regRes[2].split(" ");
      var dateChunks = dateAndTime[1].split("/");
      var timeChunks = dateAndTime[0].split(":");
      messageData["created"] = new Date(
        dateChunks[2],
        dateChunks[1] - 1,
        dateChunks[0],
        timeChunks[0],
        timeChunks[1],
        timeChunks[2],
      );
      messageLine++;
    } else if (messageLine > 1) {
      messageData["messageText"] += line;
      messageLine++;
    }
  });
  //catch last one
  messages.push(messageData);

  return {
    messages: messages,
    names: names,
  };
}

// console.log(parsed);
