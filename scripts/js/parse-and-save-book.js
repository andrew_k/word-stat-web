const lineReader = require("line-reader");
const nlp = require("../../src/tools/nlp");
const _ = require("underscore");
const Word = require("../../src/models").Word;
const async = require("async");
const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/word-stat");

const bookName = "yankee_2";

const sources = [
  `/home/akobylin/projects/word-stat-web/input_data/${bookName}`,
];

async.eachSeries(
  sources,
  function(source, sourceCb) {
    const allTokens = [];
    lineReader
      .eachLine(sources[0], function(line) {
        const tokens = nlp.tokenize(line);
        if (tokens.length === 1 && !tokens[0]) {
          return;
        }
        allTokens.push(tokens);
      })
      .then(function() {
        const flatTokens = _.flatten(allTokens);
        // const uniqTokens = _.uniq(flatTokens);
        // console.log("flatTokens", uniqTokens);
        console.log("parsed...", source);

        async.eachSeries(
          flatTokens,
          function(token, tokCb) {
            new Word({
              word: token,
              sourceName: bookName,
            }).save(tokCb);
          },
          function(err) {
            if (err) {
              console.log(err);
              return;
            }
            console.log(source, "saved", flatTokens.length);
            sourceCb(err);
          },
        );
      });
  },
  function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log("Finished!");
    }

    console.log("I'm done!!");
    mongoose.connection.close();
  },
);
