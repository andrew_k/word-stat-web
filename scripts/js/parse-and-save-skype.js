var lineReader = require("line-reader");
var fs = require("fs");
var util = require("util");
var _ = require("underscore");
var mongoose = require("mongoose");
var models = require("../models");
var Message = models.Message;
var async = require("async");

mongoose.connect("mongodb://localhost/word-stat");

var outFileName = "./skype-parsed.json";
var skypeFileName = "/home/andrew/Dropbox/Backup/skype.txt";

try {
  fs.unlinkSync(outFileName);
} catch (e) {}

var content = fs.readFileSync(skypeFileName);

var inMessage = false,
  messages = [];
messageData = {};

// console.log(content.toString('utf16le').split('\n').slice(1338, 1400));
// return;

content
  .toString("utf16le")
  .split("\n")
  .forEach(function(line, idx) {
    var line = line.trim();
    if (line === "") return;

    if (/^\=+$/.test(line)) {
      if (!inMessage) {
        inMessage = true;
      } else {
        //console.log(messageData);
        inMessage = false;
        messages.push(messageData);
        messageData = {};
      }
      return;
    }

    var regRes;
    if ((regRes = /^Record Number\s+: (.*)/.exec(line))) {
      messageData["Record Number"] = regRes[1];
    } else if ((regRes = /^Action Type\s+: (.*)/.exec(line))) {
      messageData["Action Type"] = regRes[1];
    } else if ((regRes = /^Action Time\s+: (.*)/.exec(line))) {
      messageData["Action Time"] = regRes[1];
    } else if ((regRes = /^User Name\s+: (.*)/.exec(line))) {
      messageData["User Name"] = regRes[1];
    } else if ((regRes = /^Display Name\s+: (.*)/.exec(line))) {
      messageData["Display Name"] = regRes[1];
    } else if ((regRes = /^Duration\s+: (.*)/.exec(line))) {
      messageData["Duration"] = regRes[1];
    } else if ((regRes = /^Chat Message\s+: (.*)/.exec(line))) {
      messageData["Chat Message"] = regRes[1];
    } else if ((regRes = /^ChatID\s+: (.*)/.exec(line))) {
      messageData["ChatID"] = regRes[1];
    } else if ((regRes = /^Filename\s+: (.*)/.exec(line))) {
      messageData["Filename"] = regRes[1];
    } else if (
      !/^(Record Number|Action Type|Action Time|User Name|Display Name|Duration|Chat Message|ChatID|Filename)/.test(
        line,
      )
    ) {
      messageData["chatMessage"] += line;
    }
    //console.log(idx, inMessage, line);
  });

async.each(
  messages,
  function(m, callback) {
    var message = new Message({
      userName: m["User Name"],
      displayName: m["Display Name"],
      messageText: m["Chat Message"],
      created: parseDate(m["Action Time"]),
      recipient: m["ChatID"],
      sourceName: "skype",
      data: m,
    });
    message.save(callback);
  },
  function(err) {
    if (err) console.log("Error" + err);
    console.log("finished");
    mongoose.connection.close();
  },
);

// fs.writeFileSync(outFileName, JSON.stringify(messages, null, '\t'));

function parseDate(dateString) {
  if (!dateString) return null;
  var chunks = dateString.split(" ");
  var dateChunks = chunks[0].split(".");
  var timeChunks = chunks[1].split(":");

  return new Date(
    dateChunks[2],
    dateChunks[1] - 1,
    dateChunks[0],
    timeChunks[0],
    timeChunks[1],
    timeChunks[2],
  );
}
