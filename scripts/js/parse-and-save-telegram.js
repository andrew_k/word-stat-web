const fs = require("fs");
const path = require("path");
const async = require("async");
const models = require("../../src/models/index");
const _ = require("underscore");
const { tokenize } = require("../../src/tools/nlp");

models.connect();

const targetChatName = "Myotr";
const history = JSON.parse(
  fs
    .readFileSync(
      path.resolve(
        "/home/akobylin/Downloads/Telegram Desktop/DataExport_01_05_2020/result.json",
      ),
    )
    .toString(),
);

// const targetChat = history.chats.list.find(
//   ({ name }) => name === targetChatName,
// );
//
// console.log("Name: ", targetChat.name);
// console.log("Messages: ", targetChat.messages.length);

async.forEachSeries(
  history.chats.list,
  (chat, chatCallback) => {
    async.forEachSeries(
      chat.messages,
      (message, messageCallback) => {
        // console.log(message);
        new models.TelegramMessage({
          ...message,
          words: _.isString(message.text) ? tokenize(message.text) : [],
          isText: _.isString(message.text),
          chatName: chat.name,
          chatId: chat.id,
        }).save(messageCallback);
      },
      err => {
        console.log(`Chat "${chat.name}" saved`);
        console.log(`${chat.messages.length} messages`);
        chatCallback(err);
      },
    );
  },
  err => {
    console.log("Done", err);
  },
);
