var util = require("util");
var _ = require("underscore");
var mongoose = require("mongoose");
var models = require("../models");
var async = require("async");

var Word = models.Word;
var TextEntity = models.TextEntity;
var AgregatedWord = models.AgregatedWord;

mongoose.connect("mongodb://localhost/word-stat");

async.waterfall(
  [
    function(cb) {
      TextEntity.find({
        title: "Анна Каренина",
      }).exec(function(err, result) {
        var sourceNames = _.map(result, function(ent) {
          return ent.title;
        });
        cb(err, sourceNames);
      });
    },
    function(sourceNames, waterfallCb) {
      console.log(sourceNames);
      async.eachSeries(
        sourceNames,
        function(source, sourceNameCb) {
          console.log("do aggregate for", source);

          Word.aggregate([
            {
              $match: {
                sourceName: source,
              },
            },
            {
              $group: {
                _id: "$word",
                count: {
                  $sum: 1,
                },
              },
            },
            {
              $project: {
                _id: 0,
                word: "$_id",
                count: 1,
              },
            },
          ]).exec(function(err, result) {
            console.log("words", result.length);
            if (err) console.log("Error", err);
            async.eachSeries(
              result,
              function(w, cb) {
                var aw = new AgregatedWord({
                  word: w.word,
                  count: w.count,
                  sourceName: source,
                });
                aw.save(cb);
              },
              function(err, cb) {
                console.log("saved..");
                sourceNameCb(err);
              },
            );
          });
        },
        function(err) {
          waterfallCb(err);
        },
      );
    },
  ],
  function(err) {
    console.log("waterfall finished");
    mongoose.connection.close();
  },
);
