var fs = require("fs");

var data = fs.readFileSync("./data/stem-aggr.js");
// console.log(data.toString());
var myData = eval(data.toString());

var outputFilename = "./data/json-output.js";

fs.writeFile(outputFilename, JSON.stringify(myData, null, 4), function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log("JSON saved to " + outputFilename);
  }
});
