var mongoose = require("mongoose");
var Article = require("../models").Article;
var Message = require("../models").Message;
var Word = require("../models").Word;
var async = require("async");
var _ = require("underscore");
var nlp = require("../nlp");

mongoose.connect("mongodb://localhost/word-stat");

var SOURCE_NAME = "qip";

async.series([
  function(cb) {
    Word.remove({
      sourceName: SOURCE_NAME,
    }).exec(cb);
  },
  function(cb) {
    Message.find({
      sourceName: SOURCE_NAME,
    }).exec(function(err, items) {
      // console.log(items);
      async.eachSeries(
        items,
        function(it, itCb) {
          var tokens = nlp.tokenize(it.messageText || "");

          var uniqTokens = _.uniq(tokens);
          // console.log(uniqTokens);
          async.each(
            uniqTokens,
            function(tok, tokCb) {
              var word = new Word({
                word: tok,
                created: it.created,
                sourceName: SOURCE_NAME,
                sourceId: it._id,
              });
              word.save(function(err) {
                tokCb(err);
              });
            },
            function(err) {
              itCb(err);
            },
          );
        },
        function(err) {
          if (err) {
            console.log("Error", err);
          }
          console.log("Finished!");
          mongoose.connection.close();
          cb(err);
        },
      );
    });
  },
]);

// console.log(text);
// console.log(tokenize(text));
