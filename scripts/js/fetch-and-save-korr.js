const request = require("request");
const cheerio = require("cheerio");
const async = require("async");
// const _ = require('underscore');
const models = require("../../src/models");
const mongoose = require("mongoose");
const nlp = require("../../nlp");

mongoose.connect("mongodb://localhost/word-stat");
const Article = models.Article;

// const yearStart = 2017;
// const yearEnd = 2017;
// const dayStart = 1;
// const dayEnd = 31;

const monthNames = [
  "january",
  "february",
  "march",
  "april",
  "may",
  "june",
  "july",
  "august",
  "september",
  "october",
  "november",
  "december",
];

// var month = monthNames[0];
const yearToFetch = 2017;

async.series(
  [
    cb => {
      // Article.remove({

      // }).exec(cb);
      cb();
    },
    cb => {
      fetchAndSave(monthNames.slice(0, 4), cb);
    },
  ],
  err => {
    if (err) {
      console.log("ERROR", err);
    }

    mongoose.connection.close();
  },
);

function fetchAndSave(months, fetchAndSaveCb) {
  async.eachSeries(
    months,
    (month, monthCb) => {
      getArticlesForMonth(yearToFetch, month, articles => {
        async.each(
          articles,
          (art, artCb) => {
            const article = new Article(art);
            article.save(artCb);
          },
          err => {
            if (err) {
              return monthCb(err);
            }
            return console.log(
              yearToFetch,
              ",",
              month,
              " - ",
              articles.length,
              " articles saved",
            );
          },
        );
      });
    },
    err => {
      if (err) {
        return fetchAndSaveCb(err);
      }
      return console.log("All months finished", months.join(", "));
    },
  );
}

const emptyPagesAttempt = 3;

function getArticlesForMonth(year, month, callback) {
  let pageN = 1;
  let emptyPages = 0;
  const monthArticles = [];

  function doRequest() {
    const url = `http://korrespondent.net/all/${year}/${month}/p${pageN}`;
    console.log(url);

    request(url, (err, resp, body) => {
      const $ = cheerio.load(body, {
        decodeEntities: false,
      });

      const articles = $(".article");
      console.log(articles.length);

      if (articles.length === 0) {
        emptyPages += 1;
        if (emptyPages >= emptyPagesAttempt) {
          callback(monthArticles);
        } else {
          pageN += 1;
          doRequest();
        }
      } else {
        emptyPages = 0;
        articles.each((i, art) => {
          monthArticles.push(getDataFromArticle($, art));
        });

        setTimeout(() => {
          pageN += 1;
          doRequest();
        }, 1000);
      }
    });
  }
  doRequest();
}

function getDataFromArticle($, art) {
  const createdRaw = justText($(art).find(".article__date")).trim();

  return {
    title: $(art)
      .find(".article__title a")
      .text()
      .trim(),
    text: justText($(art).find(".article__text")).trim(),
    createdRaw: createdRaw,
    created: nlp.parseDateTime(createdRaw),
    link: $(art)
      .find(".article__title a")
      .attr("href"),
  };
}

function justText(el) {
  return el
    .clone()
    .children()
    .remove()
    .end()
    .text();
}

module.export = {
  fetchMonth: () => {},
  fetchYear: () => {},
  fetchDay: () => {},
};
